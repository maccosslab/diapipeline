# Installation #
### Easiest install ###
If you don't care to develop the code here, and just want to install, the easiest way to do so is to first install Anaconda ([https://www.continuum.io/downloads](Link URL)). Then, run 
```
#!bash

conda install -c jegertso diapipeline
```
### Slightly better install ###
However, it's recommended to run the code within the same conda environment for development.
To do this, download either linux_conda_env.yml or windows_conda_env.yml from this repository, and follow the steps "Enabling the Development Environment" prior to running the above conda install command. Whenever you want to run these scripts, you'll need to enable the environment you created first.
### Docker container ###
There is also a docker container on hub.docker.com (jegertso/diapipeline).
# Development #
NOTE: Currently, only development on Linux or Windows is supported.
## Install dependencies #
#### Anaconda ###
To develop or run this pipeline, it is recommended to reproduce the python
environment (interpreter + packages) used for development and testing. The Anaconda
distribution ([https://www.continuum.io/downloads](Link URL)) is used to manage the python environment.
## Checking out the code ##
Use git to clone the repository into your project directory:
```
#!bash

git clone https://<your-username>@bitbucket.org/maccosslab/diapipeline.git diapipeline
```
## Enabling the development environment ##
Files defining windows and linux dev environments are included in the repository. To activate them, run the following:
Linux: 
```
#!bash

conda env create -n diapipeline_dev -f linux_conda_env.yml
```
Windows: 
```
#!batch

conda env create -n diapipeline_dev -f windows_conda_env.yml

```
To activate the environment, run:
Linux 
```
#!bash

source activate diapipeline_dev
```
Windows: 
```
#!batch

activate diapipeline_dev
```
To put the source packages into the python path of your environment, change into the root project directory (the one with setup.py and meta.yaml), and run:
```
#!bash

conda develop .
```
After doing this, you can run python and import packages from the source directory you area working on (ex: import DIAPipeline). The package command line binaries, however, will not be installed into your environment, you'll have to call those directly (they are in the scripts subdirectory). This can be undone by running (conda develop -u .)

For more information on conda environment, see [https://conda.io/docs/using/envs.html#use-environment-from-file](Link URL)
### Optional: enabling the dev environment in PyCharm ###
Instructions on using an anaconda environment in pycharm can be found at
[https://docs.continuum.io/anaconda/ide_integration#pycharm](Link URL)
point pycharm to the interpreter at envs/diapipeline_dev/bin/python (envs is a
subdirectory of your anaconda install)
# Packaging #
## Background ##
The code is packaged and distributed using the conda packaging system and the package repository anaconda.org. While it is possible to distribute the package using pip via python setuptools (using setup.py), this is not recommended. Anaconda / conda does a much better job at handling the intertwined dependencies of this particular project (numpy, scipy, scikit-learn, etc.).
## Install conda-build ##
### Optional: Deactivate current conda environment ###
Conda build installation must happen in the root conda environment. If you are currently working in a non-root environment (e.g. diapipeline_dev), deactivate it:
Linux: 
```
#!bash

source deactivate
```
Windows:
```
#!batch

deactivate
```
### Install conda build if not installed: ###
```
#!bash

conda install conda-build
```
### Optional: Update conda-build ###
It's a good idea to keep conda-build up to date:

```
#!bash

conda update conda-build
```
## Build the package ##
Change to the root of the project directory (the one with setup.py and meta.yaml).
Run conda build:

```
#!bash

conda build -c jegertso .
```
This will build the package and place it in a local directory on your system, for example:
/home/jegertsowork/lib64/anaconda2/conda-bld/linux-64/diapipeline-0.1.0.dev15+gd558bc7-py27_0.tar.bz2

The version string (here it's 0.1.0.dev15+gd558bc7) is defined automatically on build. It is the version number of the last package release (based on a git tag), the number of commits since that release (.dev15 == 15 commits), and part of the hash for the current commit on git (note that the "g" stands for git, the hash pattern is d558bc7 in this case). 

The package can be installed on your local machine by switching to your anaconda environment and running:
```
#!bash

conda install -c jegertso --use-local diapipeline=<version>
```
## More on packages ##
For more information on building conda packages for other python versions and environments, and sharing packages on anaconda.org: [https://conda.io/docs/build_tutorials/pkgs.html](Link URL)
# Release #
Note: Jarrett will make releases. Contact him if you need to create a new release. To do so, the code will be merged to trunk, tagged, a release branch will be created, package version numbers changed in meta.yaml and setup.py, conda packages built and uploaded to channel jegertso, and docker image built and uploaded

# Troubleshooting #
### Unknown encoding error in windows ###
Some conda commands may generate encoding errors in the default windows shell. If you see errors like "LookupError: unknown encoding: cp65001" run:

```
#!batch

set PYTHONIOENCODING=UTF-8
```
and then try running the conda command again.
More here: [https://stackoverflow.com/a/35177906](Link URL)