"""
BlibDesearleinator.py

A script to fix Brian's mistakes
"""

import pandas as pd
import struct
import zlib
from sqlalchemy import create_engine, MetaData, Index
from ..BlibUtils import BlibUtils as bu

from ..ModFix.ModFix import convert_modstring_to_blib


class Desearleinator(object):
    """
    Desearleinator corrects .blib files that are output by EncyclopeDIA. The output from EncyclopeDIA contains
    multiple reference spectra for each peptide mod seq / charge combo which is not traditionally expected in a blib.
    Desearleinator chooses a single reference spectrum for pep/charge by choosing the spectrum with the lowest q-value
    with the number of clean transitions used as a tie-breaker. It also fixes the peptide modified sequence to make the
    modification annotations compatible with Skyline.
    """
    def __init__(self, blib_file):
        self._engine = create_engine("sqlite:///%s" % blib_file)
        self._rts_sql, self._rts_dtypes = self.read_rts_table()
        self._ref_spec, self._ref_spec_dtypes = self.read_ref_spec_table()
        self._ref_spec_peaks, self._ref_spec_peaks_dtypes = self.read_ref_spectra_peaks_table()
        self._mods, self._mods_dtypes = self.read_mods_table()

    @staticmethod
    def get_dtypes(engine, tablename):
        meta = MetaData(engine, True)
        table_query = meta.tables[tablename]
        dtypes = dict()
        for col in table_query.columns:
            dtypes[col.name] = col.type
        return dtypes

    def read_rts_table(self):
        rts_df = pd.read_sql('RetentionTimes', self._engine)
        dtypes = self.get_dtypes(self._engine, 'RetentionTimes')
        return rts_df, dtypes

    def read_ref_spec_table(self):
        rs = pd.read_sql('RefSpectra', self._engine)
        dtypes = self.get_dtypes(self._engine, 'RefSpectra')
        return rs, dtypes

    def read_ref_spectra_peaks_table(self):
        rsp = pd.read_sql('RefSpectraPeaks', self._engine)
        dtypes = self.get_dtypes(self._engine, 'RefSpectraPeaks')
        return rsp, dtypes

    def read_mods_table(self):
        mods_sql = pd.read_sql('Modifications', self._engine)
        dtypes = self.get_dtypes(self._engine, 'Modifications')
        return mods_sql, dtypes

    def desearleinate(self):
        id_counter = 1
        self._ref_spec['newid'] = None
        self._ref_spec['keep'] = False
        for name, group in self._ref_spec.groupby(['peptideModSeq', 'precursorCharge']):
            self._ref_spec.loc[group.index.values, 'newid'] = id_counter
            id_counter += 1
            gsorted = group.sort_values(['score', 'numPeaks'], ascending=[True, False], na_position='last')
            # keep the lowest q value / highest num peaks
            self._ref_spec.loc[gsorted.index[0], 'keep'] = True
        self._ref_spec.set_index(['id'], inplace=True, verify_integrity=True)
        # only keep ref spec peak entries for the reference spectra
        # throw out peaks for reference spectra not being kept
        ref_spec_peaks_keep = self._ref_spec.loc[self._ref_spec_peaks['RefSpectraID'], 'keep'].values
        self._ref_spec_peaks = self._ref_spec_peaks[ref_spec_peaks_keep]
        # update the ids in the ref_spec_peaks table
        self._ref_spec_peaks.loc[:, 'RefSpectraID'] = \
            self._ref_spec.loc[self._ref_spec_peaks['RefSpectraID'], 'newid'].values

        # update the ids in the RetentionTimes table
        self._rts_sql.loc[:, 'RefSpectraID'] = self._ref_spec.loc[self._rts_sql['RefSpectraID'], 'newid'].values

        # update the mods table
        self._mods.loc[:, 'RefSpectraID'] = self._ref_spec.loc[self._mods['RefSpectraID'], 'newid'].tolist()
        self._mods.drop_duplicates(subset=['RefSpectraID', 'position', 'mass'], inplace=True)
        self._mods.loc[:, 'id'] = range(1, len(self._mods) + 1)

        # throw out redundant reference spectra
        self._ref_spec = self._ref_spec[self._ref_spec['keep']]
        self._ref_spec.reset_index(inplace=True)
        self._ref_spec.loc[:, 'id'] = self._ref_spec['newid']
        self._ref_spec.drop('newid', axis=1, inplace=True)
        self._ref_spec.drop('keep', axis=1, inplace=True)

        # convert the peptide mod seq entries to the format skyline likes
        self._ref_spec.loc[:, 'peptideModSeq'] = self._ref_spec['peptideModSeq'].apply(convert_modstring_to_blib)

    def commit(self):
        self._ref_spec.to_sql('RefSpectra', self._engine,
                              if_exists='replace',
                              dtype=self._ref_spec_dtypes,
                              index=False)

        self._rts_sql.to_sql('RetentionTimes', self._engine,
                             if_exists='replace',
                             dtype=self._rts_dtypes,
                             index=False)

        self._ref_spec_peaks.to_sql('RefSpectraPeaks', self._engine,
                                    if_exists='replace',
                                    dtype=self._ref_spec_peaks_dtypes,
                                    index=False)

        self._mods.to_sql('Modifications', self._engine,
                          if_exists='replace',
                          dtype=self._mods_dtypes,
                          index=False)
        self.write_indices()

    def write_indices(self):
        meta = MetaData(self._engine, True)
        ref_spec = meta.tables['RefSpectra']
        pep_index = Index('idxPeptide',
                          ref_spec.c.peptideSeq,
                          ref_spec.c.precursorCharge)
        pep_index.create(self._engine)
        pep_mod_index = Index('idxPeptideMod',
                              ref_spec.c.peptideModSeq,
                              ref_spec.c.precursorCharge)
        pep_mod_index.create(self._engine)
        ref_spec_peaks = meta.tables['RefSpectraPeaks']
        ref_peaks_index = Index('idxRefIdPeaks',
                                ref_spec_peaks.c.RefSpectraID)
        ref_peaks_index.create(self._engine)


class DesearleTransitionVoting(Desearleinator):
    """Take a blib output from EncyclopeDIA, and choose the "best" transitions based on a voting system. For each
    file that a transition is clean in, it gets a vote. Transitions that are clean in the most files get kept. The
    output reference spectrum sets all transition intensities to the number of votes they get"""
    def desearleinate(self):
        id_counter = 1
        self._ref_spec['newid'] = None
        self._ref_spec['keep'] = False
        for name, group in self._ref_spec.groupby(['peptideModSeq', 'precursorCharge']):
            self._ref_spec.loc[group.index.values, 'newid'] = id_counter
            id_counter += 1
            gsorted = group.sort_values(['score', 'numPeaks'], ascending=[True, False], na_position='last')
            # inject the transitions into the the reference spectrum with the lowest q value / highest num peaks
            self._ref_spec.loc[gsorted.index[0], 'keep'] = True
            # ref_peaks = self._ref_spec_peaks[self._ref_spec_peaks['RefSpectraID'].isin(group['id'].values)]
            ref_peaks = group.merge(right=self._ref_spec_peaks, how='left', left_on='id', right_on='RefSpectraID')
            ref_peaks = ref_peaks[['numPeaks', 'peakMZ']]
            self.ref_from_transition_voting(ref_peaks)

    def ref_from_transition_voting(self, ref_peaks):
        """
        Read encyclopedia reference spectra for a peptide/charge combo and choose the best transitions
        by voting.
        :param ref_peaks: pandas dataframe with refPeaks table (from blib) entries for each spectrum
                          associated with the peptide / charge combination
        :return: an encoded mz and intensity vector (intensity is number of votes)
        """
        # all_mzs = list()
        # all_mzs = [self.binary_mzs_from_blib(x) for i, x in ref_peaks.iterrows()]
        all_mzs = ref_peaks.apply(self.binary_mzs_from_blib, axis=1, broadcast=False)
        mz_counts = dict()
        for spec in all_mzs:
            for mz in spec:
                if mz not in mz_counts:
                    mz_counts[mz] = 0
                mz_counts[mz] += 1
        mz_intens = [(k, mz_counts[k]) for k in mz_counts.keys()]
        mz_vec, intens_vec = zip(*sorted(mz_intens, key=lambda x: struct.unpack('d', x[0])))
        intens_blob = bu.transform_blob(struct.pack('<' + ('f' * len(intens_vec)), *intens_vec))
        mz_blob = bu.transform_blob(''.join(mz_vec))
        return mz_blob, intens_blob

    @staticmethod
    def binary_mzs_from_blib(x):
        """
        Returns a list of mz values in binary extracted from the blobbed peak mz vector in a blib
        :param x: a pandas series with peakMZ (a blob of binary peak mzs) and numPeaks
        :return: list of mz values (binary)
        """
        peak_mz_blob = x['peakMZ']
        num_peaks = x['numPeaks']
        if len(peak_mz_blob) < (num_peaks * 8):
            peak_mz_blob = zlib.decompress(peak_mz_blob)
        # split string into 8 byte mz chunks
        print len(peak_mz_blob)
        return [peak_mz_blob[i:i+8] for i in range(0, len(peak_mz_blob), 8)]

