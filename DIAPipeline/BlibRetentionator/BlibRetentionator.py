"""
BlibRetentionator.py

A script to merge a peak boundaries file into a .blib file.
"""

import os
import pandas as pd
from sqlalchemy import create_engine, MetaData
import sys

from ..ModFix.ModFix import convert_modstring_to_pb


class BlibRetentionator(object):
    def __init__(self, blib_file, add_new):
        self._engine = create_engine("sqlite:///%s" % blib_file)
        self._add_new = add_new
        self._rts_sql, self._rts_dtypes = self.read_rts_table()
        # the default extension is the file extension of the first file in the spectrum table
        # this will be appended to any files that are added since the input data frame doesn't keep the file extension
        self._spec_source_files_sql, self._default_extension = self.read_source_files()
        # _ref_pep_to_id maps a peptide, charge combo to a peptide id
        # _id_to_ref_pep maps a refspectrum id to a peptide, charge combo
        self._ref_pep_to_id, self._id_to_ref_pep = self.get_ref_spectra_mapping()

    @staticmethod
    def get_dtypes(engine, tablename):
        meta = MetaData(engine, True)
        table = meta.tables[tablename]
        dtypes = dict()
        for col in table.columns:
            dtypes[col.name] = col.type
        return dtypes

    def read_rts_table(self):
        rts_df = pd.read_sql('RetentionTimes', self._engine).set_index(['RefSpectraID', 'SpectrumSourceID'])
        dtypes = self.get_dtypes(self._engine, 'RetentionTimes')
        return rts_df, dtypes

    def read_source_files(self):
        sf = pd.read_sql('SpectrumSourceFiles', self._engine)
        default_extension = os.path.splitext(sf['fileName'][0])[1]
        sf['fname_cleaned'] = sf['fileName'].apply(lambda x: os.path.splitext(x)[0])
        return sf.set_index('fname_cleaned'), default_extension

    def get_ref_spectra_mapping(self):
        rs = pd.read_sql('RefSpectra', self._engine)
        # currently Encyclopedia writes mods like this: [+57.0]
        # while a skyline boundaries files does this: [+57]
        # convert the blib mods to skyline boundaries file style mods (the stupid integer one)
        rs['peptideModSeq'] = rs['peptideModSeq'].apply(convert_modstring_to_pb)
        ref_pep_to_id = rs.groupby(['peptideModSeq', 'precursorCharge']).first().id
        id_to_ref_pep = rs.set_index(['id'])[['peptideModSeq', 'precursorCharge']]
        if len(id_to_ref_pep) > len(ref_pep_to_id):
            sys.stderr.write("This blib file is invalid, it has multiple ref spectrum "
                             "entries for at least one precursor." + os.linesep)
            sys.stderr.write("BlibRetentionator is just going to roll with it." + os.linesep)
            sys.stderr.write("This is almost definitely Brian's fault." + os.linesep)
        return ref_pep_to_id, id_to_ref_pep

    def process_boundaries_frame(self, df):
        peptide_not_found = 0
        row_not_added = 0
        updated = 0
        added = 0
        no_rt = 0
        sys.stderr.write("Processing boundaries             ")
        row_count = 1
        new_rows = list()
        for row in df.itertuples():
            perc_done = 100.0 * float(row_count) / float(len(df))
            sys.stderr.write("\rProcessing boundaries: %.1f%%" % perc_done)
            row_count += 1
            if row.file_name not in self._spec_source_files_sql.index:
                if not self._add_new:
                    row_not_added += 1
                    continue
                self.add_source_file(row.file_name)
            prec_key = (row.peptide_modified_sequence, row.precursor_charge)
            if prec_key not in self._ref_pep_to_id.index:
                # this reference file is in the blib, but the peptide is not
                peptide_not_found += 1
                continue
            ref_spectrum_id = self._ref_pep_to_id.loc[prec_key]
            spectrum_source_id = self._spec_source_files_sql.loc[row.file_name].id
            rt_key = (ref_spectrum_id, spectrum_source_id)
            center_rt = (row.max_end_time + row.min_start_time)/2.0
            if pd.isnull(center_rt):
                no_rt += 1
                continue
            if rt_key not in self._rts_sql.index:
                if not self._add_new:
                    row_not_added += 1
                    continue
                new_rows.append((rt_key, (0, center_rt, 1)))
                # new_row = pd.DataFrame({'RedundantRefSpectraID': [0],
                #                         'retentionTime': [center_rt],
                #                         'bestSpectrum': [1]},
                #                        index=[rt_key])
                # self._rts_sql = self._rts_sql.append(new_row)
                added += 1
            else:
                self._rts_sql.loc[rt_key, 'retentionTime'] = center_rt
                updated += 1
        new_rows_df = pd.DataFrame.from_items(new_rows,
                                              columns=('RedundantRefSpectraID', 'retentionTime', 'bestSpectrum'),
                                              orient='index')
        self._rts_sql = self._rts_sql.append(new_rows_df)
        sys.stderr.write("\rProcessing boundaries: Done     " + os.linesep +
                         "%d rows skipped because peptide not in blib" % peptide_not_found + os.linesep +
                         "%d rows skipped because peptide present but not for particular file" % row_not_added +
                         os.linesep +
                         "%d rows skipped because no peak boundaries specified" % no_rt + os.linesep +
                         "%d rows updated" % updated + os.linesep +
                         "%d rows added" % added + os.linesep)

    def add_source_file(self, file_name):
        new_id = self._spec_source_files_sql['id'].max() + 1
        new_row_df = pd.DataFrame({'id': [new_id],
                                   'fileName': file_name + self._default_extension},
                                  index=[file_name])
        self._spec_source_files_sql = self._spec_source_files_sql.append(new_row_df)

    def commit(self):
        df_to_write = self._rts_sql.reset_index().sort_values(by=['RefSpectraID', 'SpectrumSourceID'])
        df_to_write.to_sql('RetentionTimes', self._engine,
                           if_exists='replace',
                           dtype=self._rts_dtypes,
                           index=False)
