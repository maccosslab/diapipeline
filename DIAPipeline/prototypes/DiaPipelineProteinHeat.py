"""DiaPipelineProteinHeat.py

Plot a heatmap of features from a dataset.  Sorted by importance in a random forest classifier
"""

import pandas as pd
import argparse
import os
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier
import numpy as np
import seaborn as sns

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))


def init_argparse():
    parser = argparse.ArgumentParser(
        description="ProteinHeat reads in a skyline ProteinPlot report and generates a heatmap of the features "
                    "with various options for normalization --prtc and/or --zscore.  A random forest classifier "
                    "is trained to differentiate between the conditions in the dataset and used to rank the peptides "
                    "based on their contribution to the model.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--prtcNormalize', default=False, action='store_true',
                        help="Normalize the data based on PRTC peptides.")
    parser.add_argument('--zscoreNormalize', default=False, action='store_true',
                        help="Z-score normalize the data")
    parser.add_argument('inputCsv', type=os.path.abspath, help="Skyline ProteinGraph report csv")
    parser.add_argument('outputFile', type=os.path.abspath, help="Output plot file (pdf, jpg, png)")
    return parser


def get_batch(rep_name):
    return 1


def get_prtc_normalized_dataframe(df):
    # get the summed intensities of the PRTC standard peptides for each replicate
    prtc_standards = ['GISNEGQNASIK', 'HVLTSIGEK', 'TASEFDSAIAQDK', 'SAAGAFGPELSR', 'ELGQSGVDTYLQTK',
                      'GLILVGGYGTR', 'GILFVGSGVSGGEEGAR', 'SFANQPLEVVYSK', 'LTILEELR', 'NGFILDGFPR']

    # get the intensities for just the PRTC peptides
    prtc_intensities = df[(df.Protein_Name == 'PRTC') & (df.Peptide_Modified_Sequence.isin(prtc_standards))]
    norm_factors = prtc_intensities.groupby("Replicate_Name")["Total_Area_Fragment"].sum()
    norm_factors_scaled = norm_factors / norm_factors.max()
    norm_df = df.copy()
    norm_df["Total_Area_Fragment"] /= norm_factors_scaled.loc[df["Replicate_Name"]].values
    return norm_df


def zscore(x):
    return (x - x.mean()) / x.std()


def process_zscore_normalized(df):
    gb = ['Protein_Name', 'Peptide_Modified_Sequence', 'Precursor_Charge']
    if "Batch" in df.columns:
        gb.extend(["Batch"])
    df["Fragment_Z_Score"] = df.groupby(gb)['Total_Area_Fragment'].transform(zscore)


def read_skyline_report(input_file):
    df = pd.read_csv(input_file)
    cols = df.columns
    cols = cols.map(lambda x: x.replace(' ', '_') if isinstance(x, (str, unicode)) else x)
    df.columns = cols
    df["Precursor_Sequence"] = df['Peptide_Modified_Sequence'] + df['Precursor_Charge'].apply(lambda x: x * '+')
    df['Batch'] = df['Replicate_Name'].apply(get_batch)
    return df


def train_df(df):
    df_train = df.groupby(['Replicate_Name', 'Precursor_Sequence'], as_index=False).first()
    if len(df_train) < len(df):
        sys.stderr.write(
            'Warning, duplicate entries for precursors in a single replicate file were detected.' + os.linesep)
        sys.stderr.write(
            'This can happen if one peptide maps to multiple proteins. Only the first entry is considered.' +
            os.linesep)
    X = df_train.pivot(index='Replicate_Name', columns='Precursor_Sequence', values='Total_Area_Fragment')
    rep_to_cond = df_train[['Replicate_Name', 'Condition']].drop_duplicates().set_index('Replicate_Name')
    y = rep_to_cond.loc[X.index]
    clf = ExtraTreesClassifier(n_estimators=10000,
                               n_jobs=8,
                               random_state=0)
    y_fit = np.ravel(y.as_matrix())
    clf.fit(X.as_matrix(), y_fit)
    prec_seqs = X.columns.values
    importances = clf.feature_importances_
    importances_std = np.std([tree.feature_importances_ for tree in clf.estimators_], axis=0)
    prec_to_std = dict(zip(prec_seqs, importances_std))
    prec_to_importance = dict(zip(prec_seqs, importances))
    df_train.loc[:, 'Precursor_Importance'] = df['Precursor_Sequence'].apply(lambda x: prec_to_importance[x], )
    df_train.loc[:, 'Precursor_Importance_Std'] = df['Precursor_Sequence'].apply(lambda x: prec_to_std[x], )
    return df_train


def plot_heatmap(df, output_file):
    heat_df = df.pivot(index='Precursor_Sequence', columns='Replicate_Name', values='Total_Area_Fragment')
    # sort the precursors by their feature importance
    sns.set_style('whitegrid')
    sorted_rows = df[['Precursor_Sequence', 'Precursor_Importance', 'Precursor_Importance_Std']].drop_duplicates(). \
        sort_values('Precursor_Importance', ascending=False)
    new_index = sorted_rows['Precursor_Sequence']

    new_columns = df[['Replicate_Name', 'Condition']].drop_duplicates(). \
        sort_values(['Condition', 'Replicate_Name'])['Replicate_Name']
    heat_df = heat_df.reindex(new_index)
    heat_df = heat_df.reindex_axis(new_columns, axis=1)
    left, width = 0.15, 0.75
    bottom, height = 0.1, 0.8
    bottom_h = left_h = left + width + 0.02
    fig = plt.figure()
    ax_heat = plt.axes([left, bottom, width, height])
    ax_cbar = plt.axes([left, bottom_h + 0.05, width, 0.02])
    ax_feature_imp = plt.axes([left_h, bottom, 0.065, height])
    sns.heatmap(heat_df.iloc[0:100, :], ax=ax_heat, cbar_ax=ax_cbar, cbar_kws={"orientation": "horizontal"})
    ax_cbar.set_xlabel('z-score', fontsize=8, labelpad=4)
    # set colorbar sizes
    plt.setp(ax_cbar.xaxis.get_majorticklabels(), fontsize=8)
    # set heatmap sizes
    plt.setp(ax_heat.yaxis.get_majorticklabels(), rotation=0)
    plt.setp(ax_heat.yaxis.get_majorticklabels(), fontsize=4)
    plt.setp(ax_heat.xaxis.get_majorticklabels(), rotation=90)
    plt.setp(ax_heat.xaxis.get_majorticklabels(), fontsize=8)
    xerr = sorted_rows.iloc[0:100]['Precursor_Importance_Std'].values
    ax_feature_imp.barh(range(100),
                        sorted_rows.iloc[0:100]['Precursor_Importance'],
                        #                    xerr=xerr,
                        height=1.0,
                        linewidth=0)
    ax_feature_imp.set_ylim(0, 100)
    ax_feature_imp.set_yticks([])
    ax_feature_imp.set_xticks([])
    ax_feature_imp.grid(False)
    ax_feature_imp.invert_yaxis()
    plt.setp(ax_feature_imp.xaxis.get_majorticklabels(), fontsize=8)
    sns.despine(bottom=True)
    plt.savefig(output_file)
    # clust_map = sns.clustermap(heat_df.iloc[0:100,:], row_cluster=False, col_cluster=True)
    # clust_map.savefig(output_file)
    plt.close()


if __name__ == "__main__":
    parser = init_argparse()
    args = parser.parse_args()
    df = read_skyline_report(args.inputCsv)
    if args.prtcNormalize:
        df = get_prtc_normalized_dataframe(df)
    if args.zscoreNormalize:
        process_zscore_normalized(df)
        df.loc[:, "Total_Area_Fragment"] = df["Fragment_Z_Score"]
    trained_data = train_df(df.dropna())
    plot_heatmap(trained_data, args.outputFile)
