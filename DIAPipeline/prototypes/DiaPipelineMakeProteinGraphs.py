import pandas as pd
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import gridspec
import seaborn as sns
from scipy import stats
import numpy as np
import csv
import requests
import os
from statsmodels.sandbox.stats.multicomp import multipletests

CONDITION_COLUMN = "Comparison_young_old"
CONDITION_ONE = "Young"
CONDITION_TWO = "Aged"
FILE_PREFIX = "YO_comp_"


def plot_protein(df, protein_name, fig, max_peps=10, z_score_norm=True):
    # note z_score_norm assumes batch is annotated
    # ax = fig.add_subplot(121)
    sns.set_style("whitegrid")
    gs = gridspec.GridSpec(1, 2, width_ratios=[3, 1])
    ax = plt.subplot(gs[0])
    df_prot = df.loc[df.Protein_Name == protein_name]
    conditions_sorted = sorted(df_prot[CONDITION_COLUMN].unique())
    pep_order = df_prot[df[CONDITION_COLUMN] == conditions_sorted[0]] \
        .groupby("Precursor_Sequence").sum() \
        .sort_values(by="Total_Area_Fragment", ascending=False)
    pep_order = pep_order.index.get_level_values("Precursor_Sequence").tolist()[0:max_peps]
    criterion = df_prot['Precursor_Sequence'].map(lambda x: x in pep_order)
    df_plot = df_prot[criterion]
    # calculate the data frame for the protein level plot
    gb = ["Replicate_Name", CONDITION_COLUMN]
    if "Batch" in df_plot.columns:
        gb.append("Batch")
    df_prot_plot = df_plot.groupby(gb).sum()
    del df_prot_plot["Precursor_Charge"]
    if "Fragment_Z_Score" in df_prot_plot.columns:
        del df_prot_plot["Fragment_Z_Score"]
    df_prot_plot.reset_index(inplace=True)
    # calculate z_scores on the protein level
    df_prot_plot["Fragment_Z_Score"] = df_prot_plot.groupby(['Batch'])['Total_Area_Fragment'].transform(zscore)

    y_plot = "Total_Area_Fragment"
    y_label = "Intensity"
    if z_score_norm:
        y_plot = "Fragment_Z_Score"
        y_label = "z-score"

    sns.boxplot(x="Precursor_Sequence", y=y_plot, hue=CONDITION_COLUMN,
                order=pep_order,
                hue_order=conditions_sorted,
                data=df_plot,
                showfliers=False,
                palette="Set2",
                linewidth=1, ax=ax)
    sns.stripplot(x="Precursor_Sequence", y=y_plot, hue=CONDITION_COLUMN,
                  order=pep_order,
                  hue_order=conditions_sorted,
                  split=True,
                  data=df_plot,
                  size=4,
                  linewidth=1,
                  jitter=True,
                  palette="Set2", ax=ax)

    # ax = plt.gca()
    ax.set_xticklabels([entry[0:6] for entry in pep_order])
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles[0:len(conditions_sorted)], labels[0:len(conditions_sorted)], ncol=len(conditions_sorted),
              loc="upper center")
    plt.xticks(rotation=45)
    sns.despine(bottom=True)
    ax.set_xlabel("Peptide")
    ax.set_ylabel(y_label)
    accession = protein_name.split('|')[1]
    # get the recommended name for the protein
    query_string = 'http://www.uniprot.org/uniprot/?query=accession:{0:s}&columns=protein%20names&format=tab&limit=1'.format(
        accession, )
    r = requests.get(query_string)
    title = protein_name
    long_name = ""
    if r.status_code == 200:
        split_content = r.content.split('\n')
        if len(split_content) > 1:
            long_name = r.content.split('\n')[1]
            long_name = long_name.split('(')[0]
            print long_name
            title = long_name[0:50] + "\n(%s)" % protein_name

    ax.set_title(title)

    ax_prot = plt.subplot(gs[1])
    sns.boxplot(x=CONDITION_COLUMN, y=y_plot,
                order=conditions_sorted,
                data=df_prot_plot,
                showfliers=False,
                palette="Set2",
                linewidth=1, ax=ax_prot)

    sns.stripplot(x=CONDITION_COLUMN, y=y_plot,
                  order=conditions_sorted,
                  data=df_prot_plot,
                  size=4,
                  jitter=True,
                  edgecolor="gray",
                  linewidth=1,
                  palette="Set2", ax=ax_prot)
    ax_prot.set_ylabel(y_label)

    plt.tight_layout()
    return long_name


def get_protein_rank_diffs(df, protein_name, condition_one, condition_two, z_score=False):
    df_prot = df.loc[df.Protein_Name == protein_name]
    gb = ["Replicate_Name", CONDITION_COLUMN]
    if "Batch" in df_prot.columns:
        gb.append("Batch")
    df_prot_compare = df_prot.groupby(gb).sum()
    df_prot_compare.reset_index(inplace=True)
    metric = 'Total_Area_Fragment'
    if z_score:
        df_prot_compare["Fragment_Z_Score"] = df_prot_compare.groupby(['Batch'])['Total_Area_Fragment'].transform(
            zscore)
        metric = 'Fragment_Z_Score'
    rows_one = df_prot_compare[df_prot_compare[CONDITION_COLUMN] == condition_one]
    rows_two = df_prot_compare[df_prot_compare[CONDITION_COLUMN] == condition_two]
    c1_vals = rows_one[metric]
    c2_vals = rows_two[df_prot_compare[CONDITION_COLUMN] == condition_two][metric]
    if np.isnan(sum(c1_vals)) or np.isnan(sum(c2_vals)):
        return np.inf, np.inf, np.nan
    # z_stat, p_val_rsum = stats.ranksums(c1_vals, c2_vals)
    t_stat, p_val_ttest = stats.ttest_ind(c1_vals, c2_vals, equal_var=True)
    c1_mean = np.mean(rows_one['Total_Area_Fragment'])
    c2_mean = np.mean(rows_two['Total_Area_Fragment'])
    fc = np.inf
    if c2_mean != 0:
        fc = c1_mean / c2_mean
    return t_stat, p_val_ttest, fc


def get_protein_rank_rel_t(df, protein_name, condition_one, condition_two):
    df_prot = df.loc[df.Protein_Name == protein_name]
    c1_vals = df_prot[df_prot[CONDITION_COLUMN] == condition_one]["Total_Area_Fragment"]
    c2_vals = df_prot[df_prot[CONDITION_COLUMN] == condition_two]["Total_Area_Fragment"]
    if np.isnan(sum(c1_vals)) or np.isnan(sum(c2_vals)):
        return np.inf
    t_stat, p_val = stats.ttest_rel(c1_vals, c2_vals)
    return p_val


def normalize_row(row, norm_dict):
    return row["Total_Area_Fragment"] / norm_dict[row["Replicate_Name"]]


def get_prtc_normalized_dataframe(df):
    # get the summed intensities of the PRTC standard peptides for each replicate
    prtc_standards = ['GISNEGQNASIK', 'HVLTSIGEK', 'TASEFDSAIAQDK', 'SAAGAFGPELSR', 'ELGQSGVDTYLQTK',
                      'GLILVGGYGTR', 'GILFVGSGVSGGEEGAR', 'SFANQPLEVVYSK', 'LTILEELR', 'NGFILDGFPR']

    # get the intensities for just the PRTC peptides
    prtc_intensities = df[(df.Protein_Name == 'PRTC') & (df.Peptide_Modified_Sequence.isin(prtc_standards))]
    norm_factors = prtc_intensities.groupby("Replicate_Name")["Total_Area_Fragment"].sum()
    norm_factors_scaled = norm_factors / norm_factors.max()
    norm_df = df.copy()
    norm_df["Total_Area_Fragment"] /= norm_factors_scaled.loc[df["Replicate_Name"]].values
    return norm_df


def get_batch(rep_name):
    return 1


def zscore(x):
    return (x - x.mean()) / x.std()


def process_zscore_normalized(df):
    gb = ['Protein_Name', 'Peptide_Modified_Sequence', 'Precursor_Charge']
    if "Batch" in df.columns:
        gb.extend(["Batch"])
    df["Fragment_Z_Score"] = df.groupby(gb)['Total_Area_Fragment'].transform(zscore)


def write_report(output_file, df, cond1, cond2, z_score_norm=False):
    uniq_prots = sorted(df["Protein_Name"].unique(),
                        key=lambda x: get_protein_rank_diffs(df, x, cond1, cond2, z_score_norm)[1])
    sys.stderr.write("Plotting protein:                   ")
    # with open('report_statistics_%s_%s.csv' % (cond1, cond2), 'wb') as fout:
    with open(os.path.splitext(output_file)[0] + '.csv', 'wb') as fout:
        writer = csv.writer(fout)
        writer.writerow((
            "Uniprot", "Protein Name", "tStatistic", "pValue", "log2 FoldChange (%s / %s)" % (cond1, cond2),
            "Modified Bonferroni p-value", "BH FDR"))
        p_vals = list()
        rows_out = list()
        with PdfPages(output_file) as pdf:
            for i, prot in enumerate(uniq_prots):
                if prot == 'PRTC':
                    continue
                t, p, fc = get_protein_rank_diffs(df, prot, cond1, cond2, z_score_norm)
                sys.stderr.write("\rPlotting protein: %s (%d of %d)" % (prot, i + 1, len(uniq_prots)))
                fig = plt.figure()
                long_name = plot_protein(df, prot, fig, z_score_norm=z_score_norm)
                # writer.writerow((prot, long_name, t, p, np.log2(fc), bonf_p))
                rows_out.append((prot, long_name, t, p, np.log2(fc)))
                p_vals.append(p)
                pdf.savefig(fig)
                plt.close(fig)
        p_adj = multipletests(p_vals, alpha=0.05, method='holm')[1]
        fdr = multipletests(p_vals, alpha=0.05, method='fdr_bh')[1]
        for row, p_adj_, fdr_ in zip(rows_out, p_adj, fdr):
            writer.writerow(row + (p_adj_, fdr_))


if __name__ == '__main__':
    input_file = sys.argv[1]
    df = pd.read_csv(input_file)
    cols = df.columns
    cols = cols.map(lambda x: x.replace(' ', '_') if isinstance(x, (str, unicode)) else x)
    df.columns = cols
    df["Precursor_Sequence"] = df['Peptide_Modified_Sequence'] + df['Precursor_Charge'].apply(lambda x: x * '+')
    df['Batch'] = df['Replicate_Name'].apply(get_batch)
    # get unique protein names from the data
    df_norm = get_prtc_normalized_dataframe(df)
    process_zscore_normalized(df_norm)
    write_report(FILE_PREFIX + "no_normalization.pdf", df, CONDITION_ONE, CONDITION_TWO)

    write_report(FILE_PREFIX + "PRTC_normalization.pdf", df_norm, CONDITION_ONE, CONDITION_TWO)

    write_report(FILE_PREFIX + "batch_zscore.pdf", df_norm, CONDITION_ONE, CONDITION_TWO, z_score_norm=True)
