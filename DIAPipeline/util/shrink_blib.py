"""
ShrinkBlib.py

Utility script to shrink a blib file. Intended to help generate minimal
libraries for unit testing.
"""

import os
import argparse
import pandas as pd
from sqlalchemy import create_engine, MetaData
import shutil
import logging
import sys


def get_dtypes(engine, tablename):
    meta = MetaData(engine, True)
    table = meta.tables[tablename]
    dtypes=dict()
    for col in table.columns:
        dtypes[col.name] = col.type
    return dtypes

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="ShrinkBlib shrinks a blib file to a fixed number of reference spectra.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--overwrite', default=False, action='store_true',
                        help="Overwrite the output blib file if it already exists")
    parser.add_argument('N', type=int, help="Number of reference spectra to keep in output blib")
    parser.add_argument('inputBlib', type=os.path.abspath, help="Input blib")
    parser.add_argument('outputBlib', type=os.path.abspath, help="Output blib")

    args = parser.parse_args()

    if args.inputBlib == args.outputBlib:
        raise Exception("Input blib and output blib must be different")

    if os.path.exists(args.outputBlib) and not args.overwrite:
        raise Exception("Output blib %s already exists. Use the --ovewrite flag to overwrite" % args.outputBlib)

    shutil.copy(args.inputBlib, args.outputBlib)

    engine = create_engine("sqlite:///%s" % args.outputBlib)
    # trim ref spectra
    ref_spectra = pd.read_sql("RefSpectra", engine).set_index(['id'])
    if args.N > len(ref_spectra):
        logging.warning("Number of requested spectra (%d) is greater than the number of reference spectra (%d)" %
                        (args.N, len(ref_spectra)))
        logging.warning("Output blib will be a copy of the input")
        sys.exit()

    ref_spectra = ref_spectra.iloc[0:args.N, :].reset_index()
    ref_spectra_ids = ref_spectra['id']
    file_ids = ref_spectra['fileID'].unique()
    # trim ref spectra peaks
    ref_spectra_peaks = pd.read_sql("RefSpectraPeaks", engine)
    ref_spectra_peaks = ref_spectra_peaks[ref_spectra_peaks['RefSpectraID'].isin(ref_spectra_ids)]
    # trim modifications table
    modifications = pd.read_sql("Modifications", engine)
    modifications = modifications[modifications['RefSpectraID'].isin(ref_spectra_ids)]
    # retentionTimes
    retention_times = pd.read_sql("RetentionTimes", engine)
    retention_times = retention_times[retention_times['RefSpectraID'].isin(ref_spectra_ids)]
    # SpectrumSourceFiles
    spec_source_files = pd.read_sql("SpectrumSourceFiles", engine)
    spec_source_files = spec_source_files[spec_source_files['id'].isin(file_ids)]

    # write the new tables
    for data, table_name in [(ref_spectra, "RefSpectra"),
                             (ref_spectra_peaks, "RefSpectraPeaks"),
                             (modifications, "Modifications"),
                             (retention_times, "RetentionTimes"),
                             (spec_source_files, "SpectrumSourceFiles")]:
        dtypes = get_dtypes(engine, table_name)
        data.to_sql(table_name, engine,
                    if_exists='replace',
                    dtype=dtypes,
                    index=False)

    conn = engine.connect()
    conn.execute("VACUUM")
    conn.close()
