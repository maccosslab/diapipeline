import urllib
from tqdm import tqdm
from urlparse import urljoin
import os
from zipfile import ZipFile
import logging

logging.basicConfig(level=logging.INFO)


def my_hook(t):
    """
    Wraps tqdm instance. Don't forget to close() or __exit__()
    the tqdm instance once you're done with it (easiest using `with` syntax).
    """
    last_b = [0]

    def inner(b=1, bsize=1, tsize=None):
        """
        b  : int, optional
            Number of blocks just transferred [default: 1].
        bsize  : int, optional
            Size of each block (in tqdm units) [default: 1].
        tsize  : int, optional
            Total size (in tqdm units). If [default: None] remains unchanged.
        """
        if tsize is not None:
            t.total = tsize
        t.update((b - last_b[0]) * bsize)
        last_b[0] = b

    return inner


def download_file(url, output_file):
    with tqdm(unit='B', unit_scale=True, miniters=1,
              desc=url.split('/')[-1]) as t:  # all optional kwargs
        urllib.urlretrieve(url, output_file,
                           reporthook=my_hook(t))


def get_test_data(test_name, outdir=None):
    file_name = test_name + '_data.zip'
    file_url = urljoin("https://proteome.gs.washington.edu/~jegertso/diapipelinetest/", file_name)
    if outdir:
        file_name = os.path.join(outdir, file_name)
    download_file(file_url, file_name)
    logging.info("Extracting %s" % file_name)
    with ZipFile(file_name) as zf:
        zf.extractall(os.getcwd())
    logging.info("Removing %s" % file_name)
    os.remove(file_name)
    logging.info("Done")
