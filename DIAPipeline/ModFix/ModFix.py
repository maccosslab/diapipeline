"""
ModFix.py
Original Author: Jarrett Egertson
Author Date: 9/14/2016

A set of helper functions to transform peptide modified sequences to be compatible with skyline blib or peak boundary
 format.
"""

import re

# this regex will recognize modified sequences that are floating points with a decimal point in them
mod_regex = re.compile("\[\+\d+\.?\d*\]")


def transform_mod_to_pb(x):
    """
    Writes out the string modification in Skyline peak boundary format for teh modification matched by the regex match x
    :param x: a regex match
    :return: a mod string in skyline peak boundary format(ex: [+57])
    """
    return "[+%d]" % (round(float(x.group()[1:-1])))


def transform_mod_to_blib(x):
    """
    Writes out the string modification in Skyline blib format for teh modification matched by the regex match x
    :param x: a regex match
    :return: a mod string in skyline peak boundary format(ex: [+57.0])
    """
    return "[+%.1f]" % float(x.group()[1:-1])


def remove_terminus(s):
    """
    removes the * indicating protein terminus from a peptide sequnce
    :param s: input sequence ex. PEPTIDE*
    :return: fixed sequence ex. PEPTIDE
    """
    # it is ok to modify s directly since it is immutable (string) and thus passed by value
    if s[0] == '*':
        s = s[1:]
    if s[-1] == '*':
        s = s[:-1]
    return s


def convert_modstring_to_pb(s):
    """
    convert a modified sequence representation of a peptide to a skyline peak boundary format
    :param s: input mod seq string -- ex: PEPC[+57.02]TIDE*
    :return: string with fixed format -- PEPC[+57]TIDE
    """
    mod_fixed = mod_regex.sub(transform_mod_to_pb, s)
    return remove_terminus(mod_fixed)


def convert_modstring_to_blib(s):
    """
    convert a modified sequence representation of a peptide to a skyline peak boundary format
    :param s: input mod seq string -- ex: PEPC[+57.02]TIDE*
    :return: string with fixed format -- PEPC[+57]TIDE
    """
    mod_fixed = mod_regex.sub(transform_mod_to_blib, s)
    return remove_terminus(mod_fixed)

if __name__ == '__main__':
    # run a test
    input_string = "PEPC[+57.02]TIDE*"
    print "peak boundary conversion: "
    print "%s -> %s" % (input_string, convert_modstring_to_pb(input_string))
    print "blib conversion: "
    print "%s -> %s" % (input_string, convert_modstring_to_blib(input_string))

    input_string = "PEPC[+57]TIDE"
    print "peak boundary conversion: "
    print "%s -> %s" % (input_string, convert_modstring_to_pb(input_string))
    print "blib conversion: "
    print "%s -> %s" % (input_string, convert_modstring_to_blib(input_string))

    input_string = "PEPC[+57.0]TIDE"
    print "peak boundary conversion: "
    print "%s -> %s" % (input_string, convert_modstring_to_pb(input_string))
    print "blib conversion: "
    print "%s -> %s" % (input_string, convert_modstring_to_blib(input_string))

    input_string = "PEPC[+57.02]TIC[+57]DE*"
    print "peak boundary conversion: "
    print "%s -> %s" % (input_string, convert_modstring_to_pb(input_string))
    print "blib conversion: "
    print "%s -> %s" % (input_string, convert_modstring_to_blib(input_string))
