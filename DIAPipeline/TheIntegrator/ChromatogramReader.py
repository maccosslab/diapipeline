"""

hyyang
"""
import pandas as pd
import os.path
import re
import csv
from collections import namedtuple
csv.field_size_limit(200000)

class ChromatogramReader(object):
    def __init__(self):
        self.full_dataframe = None

    def add_chromatogram_file(self, chromatogram_file):
        chunksize = 1e3
        for df in pd.read_csv(chromatogram_file, sep='\t', chunksize=chunksize):
            ## the default column names in chromatogram file is different from the peak boundary from skyline
            df = df[['FileName', 'PeptideModifiedSequence', 'PrecursorCharge',
                     'FragmentIon', 'ProductCharge', 'Times', 'Intensities']]

            cols = df.columns
            cols = cols.map(
                lambda x: re.sub(r"(\w)([A-Z])", r"\1_\2", x).lower() if isinstance(x, (str, unicode)) else x)
            df.columns = cols
            df['file_name'] = df['file_name'].apply(lambda x: os.path.splitext(x)[0])

            df.set_index(
                ['file_name', 'peptide_modified_sequence', 'precursor_charge', 'fragment_ion', 'product_charge'],
                inplace=True)

            if self.full_dataframe is None:
                self.full_dataframe = df
            else:
                self.full_dataframe = self.full_dataframe.combine_first(df)

    def get_merged_dataframe(self):
        if self.full_dataframe:
            self.full_dataframe.reset_index(inplace=True)
        return self.full_dataframe


def stream_chromatograms(input_file_name):
    """
    Generator function to stream a chromatogram file rather than reading the whole thing into memory
    :param input_file_name:
    :return:
    """
    with open(input_file_name, 'rb') as fin:
        # reader + namedtuple is used here to mirror the functionality of DictReader
        # but without having to rehash the keys of the dictionary on every row read
        reader = csv.reader(fin, delimiter='\t')
        header = next(reader)
        RowTuple = namedtuple('RowTuple', header)
        current_fname = ""
        current_pep_seq = ""
        current_prec_charge = ""
        current_label_type = ""
        current_entry_transitions = list()
        fname_index = header.index('FileName')
        for row in reader:
            # store filenames without extension to match that of other data frames
            row[fname_index] = os.path.splitext(row[fname_index])[0]
            row = RowTuple(*row)
            if row.PeptideModifiedSequence != current_pep_seq or \
                            row.PrecursorCharge != current_prec_charge or \
                            row.IsotopeLabelType != current_label_type or \
                            row.FileName != current_fname:
                # just finished reading a peptide
                if len(current_entry_transitions) > 0:
                    yield current_entry_transitions
                current_fname = row.FileName
                current_pep_seq = row.PeptideModifiedSequence
                current_prec_charge = row.PrecursorCharge
                current_label_type = row.IsotopeLabelType
                current_entry_transitions = list()
            current_entry_transitions.append(row)
        # for last group in file
        if len(current_entry_transitions) > 0:
            yield current_entry_transitions
