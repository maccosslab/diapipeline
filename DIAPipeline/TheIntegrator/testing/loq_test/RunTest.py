import os
import pandas as pd
import logging
import sys
file_directory = os.path.dirname(os.path.abspath(__file__))
proj_dir = os.path.abspath(os.path.join(file_directory, '..', '..', '..', '..'))
sys.path.append(proj_dir)

from DIAPipeline.TheIntegrator.OutlierRemover import OutlierRemover
from DIAPipeline.TheIntegrator.IntegrationManager import IntegrationManager
from DIAPipeline.TheIntegrator.BoundaryImputer import SplineAlignmentImputer
from DIAPipeline.TheIntegrator.BoundaryAdjustmentManager import FixedWidthBoundaryAdjuster
from DIAPipeline.util.download_test import get_test_data

logging.basicConfig(level=logging.INFO)

if __name__ == '__main__':
    # export the automated peak boundaries from the automated mProphet analyses
    if not os.path.exists('input_data'):
        logging.info("Downloading test data")
        get_test_data('loq_test')

    auto_raw_analysis_dfs = list()
    for analysis in ('10mz', '20mz', 'demux'):
        output_report_name = os.path.join("input_data", "%s_auto_boundaries_raw.csv" % analysis)
        auto_raw_analysis_dfs.append(pd.read_csv(output_report_name))

    df = pd.concat(auto_raw_analysis_dfs)
    # remove decoys
    df = df[~df['PrecursorIsDecoy']]
    # see how many unique peptides there are before and after filtering by q value to make sure that
    # there are not precursors that are just never detected
    unique_peps = set(df['Peptide Modified Sequence'].unique())
    df['annotation_QValue'] = pd.to_numeric(df['annotation_QValue'])
    # remove precursors with q < 0.01
    df = df[df['annotation_QValue'] < 0.01]

    # special treatment for peptide C[+58]DENSPYR which has lots of false mProphet detections
    # so only keep detections that are in the 96 fmols pike in points
    dens_files = ['Q_2012_1121_RJ_%d.raw' % entry for entry in (58, 112, 166, 56, 110, 164, 59, 113, 167)]
    df = df[(df['Peptide Modified Sequence'] != 'C[+58]DENSPYR') | (df['File Name']).isin(dens_files)]

    unique_peps_filt = set(df['Peptide Modified Sequence'].unique())
    logging.info("Number of unique peptides before q value filter: %d" % len(unique_peps))
    logging.info("Number of unique peptides after q value filter: %d" % len(unique_peps_filt))
    for peptide in unique_peps - unique_peps_filt:
        logging.info("Dropped peptide: %s" % peptide)

    # rename the columns of the df
    df = df[['File Name',
             'Peptide Modified Sequence',
             'Min Start Time',
             'Max End Time',
             'Precursor Charge']]
    df.to_csv("1_peak_boundaries_filtered.csv")

    im = IntegrationManager()
    im.add_boundaries_file('1_peak_boundaries_filtered.csv')
    # now add in the peak boundaries for the background normalization peptides to seed alignment
    for analysis in ('10mz', '20mz', 'demux'):
        im.add_boundaries_file(os.path.join('input_data', 'normalization', '%s_peak_boundaries.csv' % analysis))
    merged_boundaries = im.get_merged_boundaries()
    IntegrationManager.write_boundaries_file(merged_boundaries, '2_peak_boundaries_merged.csv')

    # remove outliers from the data frame
    o_r = OutlierRemover(merged_boundaries, write_plots=True)
    outremoved_boundaries = o_r.remove_outliers()
    for peptide in unique_peps_filt - set(outremoved_boundaries['peptide_modified_sequence'].unique()):
        logging.warn("Peptide %s was removed from the dataset completely as an outlier" % peptide)
    IntegrationManager.write_boundaries_file(outremoved_boundaries, '3_peak_boundaries_outliers_removed.csv')

    # impute boundaries
    bi = SplineAlignmentImputer(outremoved_boundaries)
    imputed_boundaries = bi.impute(write_plot=True, outliers_already_removed=True)
    IntegrationManager.write_boundaries_file(imputed_boundaries, '4_peak_boundaries_imputed.csv')

    # refine the boundaries
    chromatogram_files = list()
    with open(os.path.join('input_data', 'chromatograms.txt')) as fin:
        chromatogram_files = [entry.strip() for entry in fin.readlines()]

    reference_files = list()
    with open(os.path.join('input_data', 'reference_runs.txt')) as fin:
        reference_runs = [entry.strip() for entry in fin.readlines()]

    ba = FixedWidthBoundaryAdjuster(chromatogram_files=chromatogram_files,
                                    input_df=imputed_boundaries,
                                    reference_files=reference_runs,
                                    reference_label='light',
                                    rt_tolerance=0.1
                                    )

    refined_boundaries = ba.get_boundary_refinement_result()
    IntegrationManager.write_boundaries_file(refined_boundaries, '5_peak_boundaries_refined.csv')
