import os
import logging
import sys
file_directory = os.path.dirname(os.path.abspath(__file__))
proj_dir = os.path.abspath(os.path.join(file_directory, '..', '..', '..', '..'))
sys.path.append(proj_dir)

from DIAPipeline.TheIntegrator.OutlierRemover import OutlierRemover
from DIAPipeline.TheIntegrator.IntegrationManager import IntegrationManager
from DIAPipeline.TheIntegrator.BoundaryImputer import SplineAlignmentImputer
from DIAPipeline.util.download_test import get_test_data

logging.basicConfig(level=logging.INFO)

if __name__ == '__main__':
    if not os.path.exists('input_data'):
        logging.info("Downloading test data")
        get_test_data('dia_wobble_test')

    im = IntegrationManager()
    # now add in the peak boundaries for the background normalization peptides to seed alignment
    for i in range(1, 11):
        im.add_boundaries_file(os.path.join('input_data', 'inter%d.txt' % i))
    merged_boundaries = im.get_merged_boundaries()
    IntegrationManager.write_boundaries_file(merged_boundaries, '1_peak_boundaries_merged.csv')

    # remove outliers from the data frame
    o_r = OutlierRemover(merged_boundaries, write_plots=True)
    outremoved_boundaries = o_r.remove_outliers()
    IntegrationManager.write_boundaries_file(outremoved_boundaries, '2_peak_boundaries_outliers_removed.csv')

    # impute boundaries
    bi = SplineAlignmentImputer(outremoved_boundaries)
    imputed_boundaries = bi.impute(write_plot=True, outliers_already_removed=True)
    IntegrationManager.write_boundaries_file(imputed_boundaries, '3_peak_boundaries_imputed.csv')
