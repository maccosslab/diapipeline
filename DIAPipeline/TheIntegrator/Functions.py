"""

hyyang
"""

import math
import numpy as np


def BinarySearch(list,target):
    """
    :param list: a list of value
    :param target: the target value
    :return: index of value that close to target value; int
    """
    left = 0
    right = len(list)-1
    mid = 0
    while left < right-1:
        mid = (left + right)/2
        mid = (int)(math.ceil(mid))
        if list[mid] == target:
            break
        elif list[mid] < target:
            left = mid
        else :
            right = mid
    ## it's very likely that we won't find a value exactually the same with target number
    ## so we get the one closet to target

    if np.abs(list[left] - target) > np.abs(list[right]-target):
        mid = right

    return mid


def NormalizeTrace(trace):
    """
    :param trace: intensity over time; list
    :return: intensity is normalized by sum of intensities over time; list
    """
    normalized = []
    sumint = (float)(np.sum(trace))
    if sumint == 0:
        normalized = trace
    else:
        normalized = [i/sumint for i in trace]

    return normalized


# SkylineSGFilter.paddedSavitzkyGolaySmooth(chromatogram);
def PaddedSavitzkyGolaySmooth(value):
    """
    :param value: raw intensity trace of a fragment; list
    :return: smoothed intensity; list
    """
    if value is None or len(value) == 0:
        return value
    padded = [0]* (len(value)+8)
    smoothed = padded[:]
    padded[4:-4] = value[:]
    smoothed[0:4] = padded[0:4]
    for i in range(4,len(padded)-4):
        sumValue = 59*padded[i]+54*(padded[i-1]+padded[i+1])+39*(padded[i-2]+padded[i+2])+14*(padded[i-3]+padded[i+3])-21*(padded[i-4]+padded[i+4])
        if sumValue < 0:
            sumValue = 0
        smoothed[i] = sumValue/231

    return smoothed[4:-4]




