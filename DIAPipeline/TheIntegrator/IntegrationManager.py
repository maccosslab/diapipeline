"""
IntegrationManager.py
"""

import pandas as pd
import os.path
import sys
from ..ModFix.ModFix import convert_modstring_to_pb


class IntegrationManager(object):
    def __init__(self):
        self._boundaries = dict()

    def add_boundaries_file(self, boundaries_file):
        df = pd.read_csv(boundaries_file, sep=None, engine='python')
        df = df[['File Name',
                 'Peptide Modified Sequence',
                 'Min Start Time',
                 'Max End Time',
                 'Precursor Charge']]
        # adjust the column names to be more "pythonic"
        cols = df.columns
        cols = cols.map(lambda x: x.replace(' ', '_').lower() if isinstance(x, (str, unicode)) else x)
        df.columns = cols
        df.loc[:, 'file_name'] = df['file_name'].apply(lambda x: os.path.splitext(x)[0])

        # fix notation for modifications in integration boundary file to match Skyline format
        # currently Encyclopedia writes mods like this: [+57.0]
        # while a skyline boundaries files does this: [+57]

        df['peptide_modified_sequence'] = df['peptide_modified_sequence'].apply(convert_modstring_to_pb)

        for file_name, group in df.groupby('file_name'):
            group.set_index(['peptide_modified_sequence', 'precursor_charge'], inplace=True)
            if file_name not in self._boundaries:
                self._boundaries[file_name] = group
                continue
            self._boundaries[file_name] = self._boundaries[file_name].combine_first(group)

    def get_merged_boundaries(self):
        """
        :return: a merged boundaries file w/ an entry for every precursor in every file
        """
        # reset the index on all of the boundary files and naively concatenate
        full_df = pd.concat([df.reset_index() for df in self._boundaries.values()], ignore_index=True)

        # get the list of all unique peptide, precursor, filename combinations
        new_index = list()
        filenames = sorted(self._boundaries.keys())
        full_df.drop_duplicates(inplace=True)
        for name, group in full_df.groupby(['peptide_modified_sequence', 'precursor_charge']):
            for filename in filenames:
                new_index.append((name[0], name[1], filename))
        # duplicate peptide entries warning here
        full_df.set_index(['peptide_modified_sequence', 'precursor_charge', 'file_name'],
                          verify_integrity=True, inplace=True)
        # generate the frame with the new index so every file has an entry for every peptide
        # using NaN to fill gaps
        imputed_df = full_df.reindex(new_index)
        imputed_df.reset_index(inplace=True)
        return imputed_df

    @staticmethod
    def write_boundaries_file(df, filename):
        df_subset = df[['peptide_modified_sequence',
                        'min_start_time',
                        'max_end_time',
                        'precursor_charge']]
        # add a fake PrecursorIsDecoy column
        s_length = len(df_subset)
        # df_subset.loc[:, 'precursor_is_decoy'] = pd.Series(['FALSE']*s_length, index=df_subset.index)
        decoy_column = pd.Series(['FALSE'] * s_length, index=df_subset.index)
        df_subset.insert(len(df_subset.columns), 'precursor_is_decoy', decoy_column)
        # df_subset['file_name'] = df_subset.loc[:, 'file_name'].apply(lambda x: x + '.mzML')
        new_fname = df['file_name'].apply(lambda x: x + '.mzML')
        df_subset.insert(0, 'file_name', new_fname)
        # df_subset.loc[:, 'file_name'] = df_subset.loc[:, 'file_name'].apply(lambda x: x + '.mzML')
        column_labels = ['File Name', 'Peptide Modified Sequence', 'Min Start Time', 'Max End Time',
                         'Precursor Charge', 'PrecursorIsDecoy']
        df_subset.to_csv(filename, na_rep="#N/A", header=column_labels, index=False)
