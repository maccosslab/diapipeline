"""
RtAlign.py

Author: Jarrett Egertson
"""

import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from statsmodels.regression.linear_model import OLS
from statsmodels.robust.scale import Huber
import statsmodels.api as sm
import os.path
from scipy.interpolate import interp1d, LSQUnivariateSpline
from scipy.stats import norm
import numpy as np
import seaborn as sns
from sklearn.neighbors.kde import KernelDensity
from sklearn.svm import SVR


class RTAlignment(object):
    def __init__(self, df1, df2):
        """
        Calculates an alignment between df1 and df2. Fits function f(df1_rt) -> df2_rt.
        :param df1: A data frame with file_name, peptide_modified_sequence, precursor_charge, and center_time defined
        :param df2: Same as df1
        :param write_plot: True/False if plot is to be written
        :param ax_in: Matplotlib Axes object. if set, and write_plot is True, plot will be drawn to this Axes object.
        """
        self._df1 = df1.copy()
        self._df2 = df2.copy()
        self._train_set = None
        # if the x and y coordinates of the training data are equivalent, self._identity is set to True
        self.identity = False
        self._train()

    # Implement me in any subclass! This function should set self.alignment_function to a function that
    # takes an input retention time and returns and aligned one
    def _calculate_alignment(self):
        """
        Train the aligner.
        :return:
        """
        raise NotImplementedError("_do_train is not implemented")

    def _do_align(self, rt_in):
        raise NotImplementedError("_alignment_function is not implemented")

    def _initialize_training_set(self):
        x_vals = self._df1.set_index(['peptide_modified_sequence', 'precursor_charge'])['center_time']
        x_vals.name = 'x'
        y_vals = self._df2.set_index(['peptide_modified_sequence', 'precursor_charge'])['center_time']
        y_vals.name = 'y'
        return pd.concat([x_vals, y_vals], axis=1, join='inner', verify_integrity=True).dropna().sort_values('x')

    def _train(self):
        self._train_set = self._initialize_training_set()
        if self._train_set.x.equals(self._train_set.y):
            self.identity = True
        else:
            self._calculate_alignment()

    def write_plot(self, ax_in=None):
        if ax_in:
            ax = ax_in
        else:
            fig = plt.figure()
            ax = fig.add_subplot(111)

        self._do_plot(ax)

        if not ax_in:
            fname_x = self._df1['file_name'].iloc[0]
            fname_y = self._df2['file_name'].iloc[0]
            plt.savefig('%s_%s_rtalign.pdf' % (os.path.basename(fname_x), os.path.basename(fname_y)))
            plt.close()

    def _do_plot(self, ax_in):
        fname_x = self._df1['file_name'].iloc[0]
        fname_y = self._df2['file_name'].iloc[0]
        sns.set_style('whitegrid')
        sns.regplot('x', 'y', data=self._train_set, color='black', fit_reg=False, ax=ax_in)
        x_plot = np.linspace(min(self._train_set.x) - 1, max(self._train_set.x) + 1, 500)
        y_fit = self.align(x_plot)
        ax_in.plot(x_plot, y_fit, lw=2)
        ax_in.set_xlabel("RT " + fname_x, fontsize=8)
        ax_in.set_ylabel("RT " + fname_y, fontsize=8)

    def align_one(self, rt_in):
        if np.isnan(rt_in):
            return np.nan
        if self.identity:
            return rt_in
        return self._do_align(rt_in)

    def align(self, rt_in):
        """Calculate aligned rt from an array of inputs.  Rt_in should be from file_1, rt_out is
        these rt's mapped to file_2"""
        f = np.vectorize(self.align_one)
        return f(rt_in)


class SVRAlignment(RTAlignment):
    def __init__(self, df1, df2, write_plot=False, ax_in=None, outliers_already_removed=False):
        """
        :param df1: data frame with center_rt and file_name info
        :param df2: data frame with center_rt and file_name info
        :return:
        """
        self._outliers_already_removed = outliers_already_removed
        # the minimum and maximum range for the SVR function, outside of this range
        # the linear alignment is used (extrapolated)
        self._min_svr = 0
        self._max_svr = 1000
        self._svr_fit = None
        self._f_svr = lambda x: x
        self._f_extrapolate = lambda x: x
        self._train_svr = None
        super(SVRAlignment, self).__init__(df1, df2)

    def _calculate_alignment(self):
        # trains using a support vector regressor and returns the dataframe of points
        # that will be fit by the SVR (points outside will be by linear extrapolation)
        train_set = self._train_set
        l_reg = OLS(train_set.y, sm.add_constant(train_set.x), missing='raise').fit()
        f_linear = lambda x: x * l_reg.params.x + l_reg.params.const
        if self._outliers_already_removed:
            good_vals = pd.Series(data=True, index=train_set.index)
        else:
            test = l_reg.outlier_test()
            good_vals = test.iloc[:, 2] >= 0.5

        # x and y with outliers removed and tossing bottom and top 1 percentile of points by RT
        train_svr = train_set[good_vals].sort_values('x').iloc[int(0.01 * len(good_vals)):int(0.99 * len(good_vals))]
        x_no_outl = train_svr.x
        y_no_outl = train_svr.y
        y_no_outl = y_no_outl - x_no_outl

        x_no_outl, y_no_outl = zip(*(sorted(zip(x_no_outl, y_no_outl))))
        x_array = np.matrix([(entry,) for entry in x_no_outl])
        y_array = np.array(y_no_outl)

        self._svr_fit = SVR(kernel='rbf', C=1e2, gamma=0.1, tol=1e-1).fit(x_array, y_array)
        self._f_svr = lambda x: self._svr_fit.predict(x) + x
        self._min_svr = min(x_no_outl)
        self._max_svr = max(x_no_outl)

        # the extreme ends of the SVR fit can sometimes be a bit overtrained, trim them
        svr_rt_span = self._max_svr - self._min_svr
        self._min_svr += 0.01 * svr_rt_span
        self._max_svr -= 0.01 * svr_rt_span

        # set up a function to extrapolate points outside of the SVR trained region
        # at each end, there is a linear transition from the SVR model to a simple linear regression
        # over 5% of the trained RT
        svr_margin = 0.05 * svr_rt_span
        extrap_points = [
            (self._min_svr - 2 * svr_margin, f_linear(self._min_svr - 2 * svr_margin)),
            (self._min_svr - svr_margin, f_linear(self._min_svr - svr_margin)),
            (self._min_svr, self._f_svr(np.reshape([self._min_svr], (-1, 1)))),
            (self._max_svr, self._f_svr(np.reshape([self._max_svr], (-1, 1)))),
            (self._max_svr + svr_margin, f_linear(self._max_svr + svr_margin)),
            (self._max_svr + 2 * svr_margin, f_linear(self._max_svr + 2 * svr_margin))
        ]
        extrap_x, extrap_y = zip(*extrap_points)
        self._f_extrapolate = interp1d(extrap_x, extrap_y, fill_value='extrapolate', assume_sorted=True)
        self._train_svr = train_svr

    def _do_plot(self, ax_in):
        fname_x = self._df1['file_name'].iloc[0]
        fname_y = self._df2['file_name'].iloc[0]
        sns.set_style('whitegrid')
        sns.regplot('x', 'y', data=self._train_set, fit_reg=False, ax=ax_in)
        if self._train_svr is not None:
            sns.regplot('x', 'y', data=self._train_svr, color='black', fit_reg=False, ax=ax_in)
        x_plot = np.linspace(min(self._train_set.x) - 1, max(self._train_set.x) + 1, 500)
        y_fit = self.align(x_plot)
        ax_in.plot(x_plot, y_fit, lw=2)
        ax_in.set_xlabel("RT " + fname_x, fontsize=8)
        ax_in.set_ylabel("RT " + fname_y, fontsize=8)

    def _do_align(self, rt_in):
        """Calculate the alignment for just one point"""
        if rt_in < self._min_svr or rt_in > self._max_svr:
            return float(self._f_extrapolate(np.reshape([rt_in], (-1, 1))))
        else:
            return float(self._f_svr(np.reshape([rt_in], (-1, 1))))


class KdeSplineAlignment(RTAlignment):
    def __init__(self, df1, df2, k=2, min_knot_distance=60.0):
        """
        Calculates a degree-k spline alignment between two datasets where each pointed is weighted by its kernel density
        :param df1: A data frame with file_name, peptide_modified_sequence, precursor_charge, and center_time defined
        :param df2: Same as df1
        :param k: Degree of the spline fit.
        :param min_knot_distance: The minimum distance (in seconds) between knots in the spline fit
        """
        self._k = k
        self._min_knot_distance = min_knot_distance
        # the function for the interpolated spline
        self._f_spline = None
        # function for internal kernel density estimate given x and y vector
        self._f_kde = None
        # function for weight calculation given x and y vector
        self._f_weight = None
        # this will initialize and train the alignment
        super(KdeSplineAlignment, self).__init__(df1, df2)

    def _calculate_weights(self, train_set):
        # TODO: add CV bandwidth selection
        # TODO: calculate bandwidth based on distance distribution of sorted x-values
        # TODO: plot KDE estimate
        # calculate distances between points from one of the files (train_set is sorted by 'x')
        distances = np.ediff1d(train_set['x'].values)
        # set the bandwidth of the kernel density estimate to be the 90th percentile of distances between points
        bw = np.percentile(distances, 90)
        kde_data = train_set[['x', 'y']]

        # rtol=0.01 is a performance speed up that tells the kde estimator that the calculated density
        # needs to be within 1% of the true density
        self._f_kde = KernelDensity(kernel='gaussian', bandwidth=bw, rtol=0.01)
        self._f_kde.fit(kde_data)
        # the weights are the densities, the kde function returns log weights, so np.exp is used to reverse that
        self._f_weights = lambda wd: np.exp(self._f_kde.score_samples(wd))

        if len(train_set) < 5:
            return self._f_weights(kde_data)

        # calculate a robust estimate of the mean and standard deviation of the delta RT values from the input dataset
        try:
            mean, std = Huber(maxiter=100)((train_set.y - train_set.x).values)
        except ValueError:
            mean, std = norm.fit((train_set.y - train_set.x).values)

        # weights are the weight from the kernel density estimate multiplied by
        # the gaussian pdf of the observed delta RT of each point -- the gaussian pdf is transformed to have standard
        # deviation of 5x the estimate such that most of the points without outliers are weighted roughly equally
        # but there is a dropoff outside of this range
        self._f_weights = lambda wd: np.exp(self._f_kde.score_samples(wd)) * norm.pdf((wd.y - wd.x).values, mean, std * 5.0)
        return self._f_weights(kde_data)

    def _get_knots(self, rt_array):
        """
        :param rt_array: array of retention times on alignment x axis from which to calculate knot locations
        :return: list of retention times for knots
        """
        knot_min = self._min_knot_distance/60.0
        knots = list()
        prev_ind = 0
        ind = 10
        while ind < len(rt_array)-10:
            if rt_array[ind] - rt_array[prev_ind] > knot_min:
                knots.append(rt_array[ind])
                prev_ind = ind
                ind += 10
            else:
                ind += 1
        return knots

    def _calculate_alignment(self):
        x_array = self._train_set.x.values
        y_array = self._train_set.y.values
        # knots = x_array[5:-5:10]
        knots = self._get_knots(x_array)
        weights = self._calculate_weights(self._train_set)
        self._f_spline = LSQUnivariateSpline(x_array, y_array, t=knots, w=weights, k=2)

    def _do_align(self, rt_in):
        return self._f_spline(rt_in)

    def _do_plot(self, ax_in):
        fname_x = self._df1['file_name'].iloc[0]
        fname_y = self._df2['file_name'].iloc[0]
        sns.set_style('white')

        # calculate a square viewport for the plot
        min_point = self._train_set[['x', 'y']].values.min()
        max_point = self._train_set[['x', 'y']].values.max()
        plot_margin = 0.05*(max_point - min_point)
        min_point -= plot_margin
        max_point += plot_margin
        # calculate a grid for the KDE estimate
        xx, yy = np.mgrid[min_point:max_point:100j, min_point:max_point:100j]
        grid_positions = pd.DataFrame(data={'x': xx.ravel(), 'y': yy.ravel()})
        f = np.reshape(self._f_weights(grid_positions), xx.shape)
        ax_in.contourf(xx, yy, f, 10, cmap='Blues')

        sns.regplot('x', 'y', data=self._train_set, color='black', fit_reg=False, ax=ax_in, marker='.')
        x_plot = np.linspace(min(self._train_set.x) - 1, max(self._train_set.x) + 1, 500)
        y_fit = self.align(x_plot)
        ax_in.plot(x_plot, y_fit, lw=2)
        ax_in.set_xlabel("RT " + fname_x, fontsize=8)
        ax_in.set_ylabel("RT " + fname_y, fontsize=8)

        ax_in.set_xlim((min_point, max_point))
        ax_in.set_ylim((min_point, max_point))

