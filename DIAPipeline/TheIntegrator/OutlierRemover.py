"""
OutlierRemover.py

Author: Jarrett Egertson
Date: 5/9/2016
"""

import numpy as np
import pandas as pd
from collections import namedtuple
from RtAlign import KdeSplineAlignment
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
from BoundaryImputer import BoundaryImputer

NormalDistrib = namedtuple("NormalDistrib", ['mean', 'std'])


class OutlierRemover(object):
    def __init__(self, input_df, write_plots=False):
        self._original_columns = input_df.columns
        self._input_df = self.add_center_rt(input_df)
        self._input_df = self.align_rts(self._input_df, write_plots=write_plots)
        # fit a normal distribution to the delta retention time for each pair of files
        # an effort is made to eliminate outliers prior to fitting the distribution
        # by assuming a 1% FDR
        self._rt_distributions = self.get_rt_distribution_matrix(self._input_df, aligned=True)

    @staticmethod
    def align_rts(df, write_plots=False):
        """
        calculates aligned center, start, and end RTs and adds them to the input dataframe
        :param write_plots: outputs a pdf of unaligned and aligned RTs
        :return:
        """
        # choose the anchor file for RTs -- the one with the lowest average RT diff compared to teh other files
        # rt_distribs = OutlierRemover.get_rt_distribution_matrix(df, aligned=False, abs_delta=True).applymap(np.mean)
        # anchor_file = rt_distribs.sum().idxmin()

        # choose the anchor file as the one with the most non-nan peptides
        anchor_file = df.dropna().groupby('file_name').count()['peptide_modified_sequence'].idxmax()
        anchor_df = df[df['file_name'] == anchor_file]
        # calculate an alignment for each input dataframe to the anchor
        new_df = df.groupby('file_name', as_index=False).apply(lambda x: OutlierRemover.get_aligned_df(x, anchor_df))
        if write_plots:
            fig = plt.figure(figsize=(8, 6), dpi=300)
            sns.set_style('whitegrid')
            ax_unalign = fig.add_subplot(211)
            ax_align = fig.add_subplot(212, sharex=ax_unalign)
            for name, group_df in new_df.groupby('file_name'):
                plot_df = group_df.merge(anchor_df,
                                         on=['peptide_modified_sequence', 'precursor_charge'],
                                         how='inner',
                                         suffixes=('', '_anchor')).sort_values(['center_time_anchor'])
                ax_unalign.plot(plot_df['center_time_anchor'], plot_df['center_time'], '.', markersize=2)
                ax_unalign.set_xlabel('Reference File Retention Time')
                ax_unalign.set_ylabel('Retention Time')

            ax_unalign.set_title('unaligned RTs')

            for name, group_df in new_df.groupby('file_name'):
                plot_df = group_df.merge(anchor_df,
                                         on=['peptide_modified_sequence', 'precursor_charge'],
                                         how='inner',
                                         suffixes=('', '_anchor')).sort_values(['center_time_anchor'])
                ax_align.plot(plot_df['center_time_anchor'], plot_df['aligned_center_time'], '.', markersize=2)
                ax_align.set_xlabel('Reference File Retention Time')
                ax_align.set_ylabel('Aligned Retention Time')
            ax_align.set_title('aligned RTs')
            plt.tight_layout()
            plt.savefig('outlier_removal_rt_comparison.png', dpi=300)
            plt.close()
        return new_df

    @staticmethod
    def get_aligned_df(to_align_df, anchor_df):
        """
        :param to_align_df: a dataframe with center time, min start time, max end time entries for just one file
        :param anchor_df: a dataframe with reference retention times to align to
        :return:
        """
        aligner = KdeSplineAlignment(to_align_df, anchor_df)
        new_cols = to_align_df[['center_time', 'min_start_time', 'max_end_time']].apply(aligner.align)
        new_cols.columns = ['aligned_center_time', 'aligned_min_start_time', 'aligned_max_end_time']
        ret_df = to_align_df.join(new_cols)
        return BoundaryImputer.correct_start_stop_order(ret_df, start_col='aligned_min_start_time',
                                                        end_col='aligned_max_end_time')

    def remove_outliers(self):
        no_outliers = self._input_df.groupby(['peptide_modified_sequence', 'precursor_charge']).apply(
            self.scrub_outliers)
        return no_outliers[self._original_columns]

    @staticmethod
    def check_overlap(a, b):
        return min(a[1], b[1]) - max(a[0], b[0]) > 0

    def scrub_outliers(self, df, criteria='delta_rt'):
        """
        :param df: a dataframe containing columns center_time, file_name, min_start_time, and max_end_time
        :param criteria: how to tell if a pair of peaks are different -- delta_rt checks if the observed delta
        retention time is within 3 sigma of expectation, overlaps sees if the peaks' boundaries overlap with eachother
        after correcting for average delta rt between the pair of files
        :return: a new dataframe with min_start_time, max_end_time and center_time olumns w/ outliers removed
        """
        df_fit = df.dropna(subset=['center_time'])
        if len(df_fit) <= 1:
            return df

        start_col_name = 'min_start_time'
        center_col_name = 'center_time'
        end_col_name = 'max_end_time'

        aligned = False
        if 'aligned_center_time' in df.columns:
            # use aligned times if they exist
            start_col_name = 'aligned_min_start_time'
            center_col_name = 'aligned_center_time'
            end_col_name = 'aligned_max_end_time'
            aligned = True

        # find the largest cluster of neighboring rt values
        df_fit = df_fit.sort_values([center_col_name])[['file_name', start_col_name, center_col_name, end_col_name]]
        biggest_cluster = 0
        biggest_start = 0
        current_cluster = 0
        current_start = 0
        for i in range(len(df_fit) - 1):
            pair = df_fit.iloc[[i, i + 1]]
            file_1, file_2 = pair['file_name'].values
            rt_1, rt_2 = pair[center_col_name].values
            delt_rt = rt_1 - rt_2
            delt_min, delt_max = self._rt_distributions[file_1][file_2]
            # if delt_rt is not within this min/max it's a bad one mark as boundary
            if criteria == 'delta_rt':
                pass_criteria = delt_min < delt_rt < delt_max
            elif criteria == 'overlap':
                average_delta_rt = (delt_min + delt_max) / 2.0
                start_1, start_2 = pair[start_col_name].values
                end_1, end_2 = pair[end_col_name].values
                # correct for constant RT shift
                if not aligned:
                    start_1 -= average_delta_rt
                    end_1 -= average_delta_rt
                pass_criteria = self.check_overlap((start_1, end_1), (start_2, end_2))
            else:
                raise ValueError("Criteria %s is not recognized, options are: delta_rt and overlap" % criteria)

            if pass_criteria:
                current_cluster += 1
            else:
                if current_cluster > biggest_cluster:
                    biggest_cluster = current_cluster
                    biggest_start = current_start
                current_cluster = 0
                current_start = i + 1
        if current_cluster > biggest_cluster:
            biggest_cluster = current_cluster
            biggest_start = current_start

        if biggest_cluster + 1 == len(df_fit):
            # no outliers
            return df

        # if the largest cluster of points is not the majority, set all to outliers
        outlier_mask = np.array([True] * len(df_fit))
        if biggest_cluster + 1 > len(df_fit) / 2.0:
            biggest_end = biggest_start + biggest_cluster + 1
            outlier_mask[biggest_start:biggest_end] = False
        outlier_indices = df_fit[outlier_mask].index.values
        df.loc[outlier_indices, ['center_time', 'min_start_time', 'max_end_time']] = np.nan
        return df

    @staticmethod
    def agg_func_with_exception(x):
        if len(x) > 1:
            raise Exception("Error creating pivot table -- multiple entries for column")
        return x.iloc[0]

    @staticmethod
    def get_rt_distribution_matrix(df, aligned=False, abs_delta=False):
        """
        for each pair of runs, calculate the mean and standard deviation of the distribution
        of abs(delta retention times), with the worst 1% of delta RTs tossed (as expected
        by encyclopedia 1% FDR).  The matrix is populated with the +/- 3 sigma values of the
        delta RT.  Delta RT is calculated as row entry - column entry.
        :param aligned:  if aligned is true, the aligned center time will be used to calculate the distribution
        :param abs_delta: if True, distribution if so absolute value of delta RT
        """
        center_col = 'center_time'
        if aligned:
            center_col = 'aligned_center_time'
        pivot = pd.pivot_table(df,
                               values=center_col,
                               index=['peptide_modified_sequence', 'precursor_charge'],
                               columns='file_name',
                               aggfunc=OutlierRemover.agg_func_with_exception)

        return pivot.apply(
            lambda col1: pivot.apply(
                lambda col2: OutlierRemover.get_rt_distribution_by_columns(col1, col2, abs_delta=abs_delta)))

    @staticmethod
    def get_rt_distribution_by_columns(col1, col2, abs_delta):
        delta_rts = col1 - col2
        delta_df = pd.concat([delta_rts, np.absolute(delta_rts)], axis=1, keys=['delta_rt', 'abs_delta_rt']).dropna()
        # toss out the worst delta rts based on waving hands and a 1% FDR of detection
        keep = int(0.99 * 0.99 * len(delta_df.index))
        delt_column = 'delta_rt'
        if abs_delta:
            delt_column = 'abs_delta_rt'
        delta_rt_fit = delta_df.sort_values('abs_delta_rt', ascending=True)[delt_column][:keep]
        mean = np.mean(delta_rt_fit)
        stdev = np.std(delta_rt_fit)
        return mean - 3 * stdev, mean + 3 * stdev

    @staticmethod
    def add_center_rt(df):
        df_ret = df.copy()
        df_ret['center_time'] = (df.min_start_time + df.max_end_time) / 2.0
        return df_ret
