"""
DevAligners.py

This is RT alignment code that is in a development state and is mostly abandoned.  Leaving it here because it may
be useful later.
"""

from six import string_types
import statsmodels.nonparametric.api as nparam
from scipy import optimize
from statsmodels.regression.linear_model import OLS
import seaborn as sns
import pandas as pd
import statsmodels.api as sm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os


class KernelAligner(object):
    def __init__(self, df, fname1, fname2):
        """
        :param df: data frame with center_rt and file_name info
        :param fname1: first file in alignment
        :param fname2: second file in alignment
        :return:
        """
        self._df = df
        self._fname1 = fname1
        self._fname2 = fname2
        self._f_x = None
        self.train()

    def train(self, write_plot=False):
        x_vals = self._df[self._df['file_name'] == self._fname1].set_index(
            ['peptide_modified_sequence', 'precursor_charge'])['center_time']
        x_vals.name = 'x'
        y_vals = self._df[self._df['file_name'] == self._fname2].set_index(
            ['peptide_modified_sequence', 'precursor_charge'])['center_time']
        y_vals.name = 'y'
        train_set = pd.concat([x_vals, y_vals], axis=1, join='inner', verify_integrity=True).dropna()
        # perform a linear regression to determine outliers
        l_reg = OLS(train_set.y, sm.add_constant(train_set.x), missing='raise').fit()
        test = l_reg.outlier_test()
        good_vals = test.iloc[:, 2] >= 0.5

        apply_l_reg = lambda x: x * l_reg.params.x + l_reg.params.const
        # x and y with outliers removed
        x_no_outl = train_set.x[good_vals]
        y_no_outl = train_set.y[good_vals]

        x_no_outl, y_no_outl = zip(*(sorted(zip(x_no_outl, y_no_outl))))

        model = nparam.KernelReg(endog=y_no_outl, exog=x_no_outl, reg_type='ll', var_type='c', bw='aic')
        mean_vals, mfx = model.fit()
        # points outside of the range of the loess estimation are interpolated between the last loess point
        # and the linear regression line
        if write_plot:
            sns.set_style('whitegrid')
            ax = sns.regplot('x', 'y', data=train_set, line_kws={'lw': 1, 'color': 'red'}, fit_reg=False)
            ax.plot(x_no_outl, mean_vals, lw=2)
            plt.savefig('%s_%s_rtalign.pdf' % (os.path.basename(self._fname1), os.path.basename(self._fname2)))
            plt.close()

    def align(self, rt_in):
        """Calculate aligned rt from an array of inputs.  Rt_in should be from file_1, rt_out is
        these rt's mapped to file_2"""
        return 5


def statsmodels_bv_kde(x, y, bw='scott'):
    """Function derived from seaborn.distributions._statsmodels_bivariate_kde to
    calculate a kde model.
    :param x: x values (vector or 1d array)
    :param y: y values (vector or 1d array)
    :param bw: Options for bw are: same as seaborn.kdeplot --
    'scott'|'silverman'|scalar|pair of scalars"""
    if isinstance(bw, string_types):
        bw_func = getattr(nparam.bandwidths, "bw_" + bw)
        x_bw = bw_func(x)
        y_bw = bw_func(y)
        bw = [x_bw, y_bw]
    elif np.isscalar(bw):
        bw = [bw, bw]

    if isinstance(x, pd.Series):
        x = x.values

    if isinstance(y, pd.Series):
        y = y.values

    kde = nparam.KDEMultivariate([x, y], "cc", bw)
    # call kde.pdf(data_predict=[ndarray, ndarray]
    return kde


class KernelModeAligner(object):
    def __init__(self, df, fname1, fname2):
        """
        :param df: data frame with center_rt and file_name info
        :param fname1: first file in alignment
        :param fname2: second file in alignment
        :return:
        """
        self._df = df
        self._fname1 = fname1
        self._fname2 = fname2
        self._f_x = None
        self.train()

    def train(self, write_plot=False):
        x_vals = self._df[self._df['file_name'] == self._fname1].set_index(
            ['peptide_modified_sequence', 'precursor_charge'])['center_time']
        x_vals.name = 'x'
        y_vals = self._df[self._df['file_name'] == self._fname2].set_index(
            ['peptide_modified_sequence', 'precursor_charge'])['center_time']
        y_vals.name = 'y'
        train_set = pd.concat([x_vals, y_vals], axis=1, join='inner', verify_integrity=True).dropna()
        kde = statsmodels_bv_kde(train_set.x, train_set.y, bw=1.0)
        y_start = 35.0
        while y_start < 45.0:
            print y_start, kde.pdf([np.array([40.0]), np.array([y_start])])
            y_start += 0.1

        fit_y = [
            optimize.minimize_scalar(lambda x_: -1.0 * kde.pdf(data_predict=[[t_x], [x_]]), bounds=(t_x - 5, t_x + 5),
                                     ).x for t_x in train_set.x]
        print fit_y
        # points outside of the range of the loess estimation are interpolated between the last loess point
        # and the linear regression line
        if write_plot:
            sns.set_style('whitegrid')
            ax = sns.kdeplot(train_set.x, train_set.y, shade=True, bw=[5.0, 1.0])
            sns.regplot('x', 'y', data=train_set, line_kws={'lw': 1, 'color': 'red'}, fit_reg=False, ax=ax)
            ax.plot(train_set.x, fit_y, 'k.')
            plt.savefig('%s_%s_rtalign.pdf' % (os.path.basename(self._fname1), os.path.basename(self._fname2)))
            plt.close()

    def align(self, rt_in):
        """Calculate aligned rt from an array of inputs.  Rt_in should be from file_1, rt_out is
        these rt's mapped to file_2"""
        return 5