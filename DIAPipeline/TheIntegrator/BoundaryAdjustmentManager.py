import Functions
import numpy as np
import pandas as pd
import sys
from ChromatogramReader import stream_chromatograms
from bisect import bisect_left
import scipy.stats as st
from scipy.stats import linregress
from ..ModFix.ModFix import convert_modstring_to_pb
from math import sqrt
import os

def get_idx_closest(sorted_list, x, lo=0):
    index = bisect_left(sorted_list, x, lo=lo)
    if index == 0:
        return index
    if index == len(sorted_list):
        return index - 1
    if abs(x - sorted_list[index]) < abs(x - sorted_list[index-1]):
        return index
    return index-1


class FixedWidthBoundaryAdjuster(object):
    def __init__(self, chromatogram_files, input_df, reference_files, rt_tolerance=0.5, reference_label='light'):
        """
        Searches in the local RT region for a new peak but keeps peak integration at a fixed width.
        :param chromatogram_files: A list of chromatogram file locations to process
        :param input_df: a dataframe of peak boundaries
        :param reference_files: a list of file names to use as references for peak width and relative fragment
                                intensities
        :param rt_tolerance: the local region to search for a new peak center. New peaks will be searched within
                             input peak center +/- rt_tolerance
        :param reference_label: 'light' or 'heavy' to indicate which isotope label type should be used for reference
        """
        self._chromatogram_files = chromatogram_files
        self._input_df = input_df
        self._reference_files = reference_files
        self._rt_tolerance = rt_tolerance
        self._ref_spectra = self._get_reference_spectra(chromatogram_files, input_df, reference_files, reference_label)

    @staticmethod
    def _get_reference_spectra(chromatogram_files, input_df, reference_files, reference_label):
        """
        Extracts reference spectra and peak widths from the chromatogram file for each peptide in the input data frame
        by extracting the signal between the peak boundaries (defined in input_df) from the chromatograms file
        for each peptide.
        :param chromatogram_files: a list of chromatogram file paths to process
        :param input_df: peak boundaries data frame
        :param reference_files: list of files to use as references for creating reference spectra from chromatograms
        :param reference_label: which isotope label to use for reference: light or heavy
        :return: reference spectra dataframe with columns peptide_modified_sequence, precursor_charge,
                 fragment_ion (ex. b3), product_charge, peak_width, intensity
        """
        ref_set = set(reference_files)
        ref_boundaries = input_df.groupby(['file_name', 'peptide_modified_sequence']).first()[['min_start_time', 'max_end_time']]
        reference_spectra = list()
        # keep FileName, PeptideModifiedSequence, PrecursorCharge, FragmentIon, ProductCharge, Times, Intensity
        for chrom_file in chromatogram_files:
            for chrom_set in stream_chromatograms(chrom_file):
                # only consider chromatograms that are part of a reference file and are the reference isotope label type
                if len(chrom_set) == 0 or \
                                chrom_set[0].FileName not in ref_set or \
                                chrom_set[0].IsotopeLabelType != reference_label:
                    continue
                # get peak start / stop for this peptide
                # File Name, Peptide Modified Sequence
                ref_query = (chrom_set[0].FileName, convert_modstring_to_pb(chrom_set[0].PeptideModifiedSequence))
                try:
                    ref_start, ref_stop = ref_boundaries.loc[ref_query]
                except KeyError:
                    # the peptide + file combination is in the chromatograms file but not in the reference
                    # peak boundaries data frame
                    continue

                times = [float(i) for i in chrom_set[0].Times.split(',')]
                # get the indices of the time values closest to the start and stop of the peak
                start_idx = get_idx_closest(times, ref_start)
                stop_idx = get_idx_closest(times, ref_stop, max((0, start_idx-1)))
                peak_width = times[stop_idx] - times[start_idx]
                # for each transition, calculate the integrated intensity and add it to the reference list
                for transition in chrom_set:
                    integrated_intensity = sum([float(entry) for entry in transition.Intensities.split(',')[start_idx:stop_idx+1]])
                    reference_spectra.append((transition.FileName,
                                              convert_modstring_to_pb(transition.PeptideModifiedSequence),
                                              transition.PrecursorCharge,
                                              transition.FragmentIon,
                                              transition.ProductCharge,
                                              peak_width,
                                              integrated_intensity))
        spectra_df_cols = ["file_name", "peptide_modified_sequence", "precursor_charge", "fragment_ion",
                           "product_charge", "peak_width", "intensity"]
        ref_spectra_df = pd.DataFrame.from_records(reference_spectra, columns=spectra_df_cols)
        ref_spectra_df[['precursor_charge', 'product_charge']] = ref_spectra_df[['precursor_charge', 'product_charge']].apply(pd.to_numeric)
        # average the reference spectra over multiple files, keep the max peak width
        ref_spectra_df = ref_spectra_df.groupby(['peptide_modified_sequence', 'precursor_charge',
                                                 'fragment_ion', 'product_charge']).agg({'peak_width': 'max',
                                                                                         'intensity': 'mean'})
        return ref_spectra_df

    def get_boundary_refinement_result(self):
        # stream through teh set of chromatograms again if not a reference do the local search
        # for each peptide / file combination pull up the chromatograms
        ref_set = set(self._reference_files)
        # if multiple precursor charge states, pick the peak boundaries from the first one, since the boundaries
        # should not vary between charge states
        unrefined_boundaries = self._input_df.groupby(['file_name', 'peptide_modified_sequence']).first()[['min_start_time', 'max_end_time']]
        refined_boundaries = self._input_df.set_index(['file_name', 'peptide_modified_sequence', 'precursor_charge'])
        refined_boundaries.sort_index(inplace=True)
        sys.stderr.write(os.linesep)
        counter = 0
        for chrom_file in self._chromatogram_files:
            for chrom_set in stream_chromatograms(chrom_file):
                counter += 1
                sys.stderr.write("\rProcessing peak: %d       " % counter)
                if len(chrom_set) == 0:
                    continue
                # get peak start / stop for this peptide
                # File Name, Peptide Modified Sequence
                query_filename = chrom_set[0].FileName
                query_pep_mod_seq = convert_modstring_to_pb(chrom_set[0].PeptideModifiedSequence)
                try:
                    unrefined_start, unrefined_stop = unrefined_boundaries.loc[query_filename, query_pep_mod_seq]
                except KeyError:
                    # the peptide + file combination is in the chromatograms file but not in the reference
                    # peak boundaries data frame
                    continue
                query_prec_charge = int(chrom_set[0].PrecursorCharge)
                ref_spectrum = self._ref_spectra.loc[query_pep_mod_seq, query_prec_charge]
                ref_peak_width = ref_spectrum['peak_width'][0]
                times = [float(i) for i in chrom_set[0].Times.split(',')]
                # get the indices of the time values to consider within the retention time tolerance
                unrefined_center = (unrefined_start + unrefined_stop)/2.0
                if np.isnan(unrefined_center):
                    sys.stderr.write(
                        os.linesep + "Peptide %s has undefined boundaries and is removed." % query_pep_mod_seq + os.linesep)
                    continue
                if unrefined_center > times[-1]:
                    # this can happen if a chromatograms file contains truncated chromatograms
                    continue

                start_idx = get_idx_closest(times, unrefined_center - ref_peak_width/2.0 - self._rt_tolerance)
                stop_idx = get_idx_closest(times, unrefined_center + ref_peak_width/2.0 + self._rt_tolerance, max((0, start_idx-1)))

                if stop_idx == len(times)-1:
                    # this can happen if a chromatograms file contains truncated chromatograms
                    sys.stderr.write(os.linesep +
                                     "Skipping peptide %s with center %f because it is at the end of the chromatogram" %
                                     (query_pep_mod_seq, unrefined_center) + os.linesep)
                    continue
                rt_per_idx = (times[stop_idx] - times[start_idx]) / (stop_idx - start_idx)
                idx_peak_width = int(round(ref_peak_width / rt_per_idx))
                df_dict = {}
                for transition in chrom_set:
                    intens = [float(entry) for entry in transition.Intensities.split(',')[start_idx:stop_idx+1]]
                    df_dict[(transition.FragmentIon, int(transition.ProductCharge))] = intens
                peptide_chroms = pd.DataFrame(df_dict, index=range(start_idx, stop_idx+1))
                # optimal_index = self._find_optimal_index(peptide_chroms, ref_spectrum.intensity, idx_peak_width,
                #                                          score_func=self.spectral_contrast)
                optimal_index = self._find_optimal_index(peptide_chroms, ref_spectrum.intensity, idx_peak_width,
                                                         score_func=self.lsq_fit)
                if np.isnan(optimal_index):
                    # this can happen if all of the MS2 data for the peptide is 0
                    continue
                new_center_rt = times[optimal_index]
                new_start_rt, new_end_rt = new_center_rt - ref_peak_width/2.0, new_center_rt + ref_peak_width/2.0
                refined_boundaries.loc[query_filename, query_pep_mod_seq]['min_start_time'] = new_start_rt
                refined_boundaries.loc[query_filename, query_pep_mod_seq]['max_end_time'] = new_end_rt
                # TODO: potentially make this faster by storing these, converting to dataframe and running combine first
        sys.stderr.write(os.linesep)
        return refined_boundaries.reset_index()

    @staticmethod
    def _find_optimal_index(peptide_data, reference_spectrum, idx_peak_width, score_func):
        """
        :param peptide_data: each row is a spectrum, columns are fragment ions
        :param reference_spectrum: a series with index (fragment, frag charge) and intensity values
        :param idx_peak_width: of the peak in index units (number of rows in peptide_data)
        :param score_func: a score function to compare two spectra (reference and observed). Maximum of this score vs
                           RT is used to determine peak center.
        :return: the index of the maximum location
        """
        #TODO update teh text here to explain score_func and convolution with gaussian kernel
        scores = peptide_data.apply(score_func, axis=1, args=(reference_spectrum,))
        """
        fig = plt.figure()
        ax = fig.add_subplot(111)
        for col in peptide_data:
            ax.plot(peptide_data[col])
        ax2 = ax.twinx()
        ax2.plot(scores, color='black', lw=3)
        rolling_avg = scores.rolling(window=idx_peak_width/5, center=True, min_periods=1).mean()
        ax2.plot(rolling_avg, color='black', ls='--', lw=3)
        ax2.axvline(rolling_avg.idxmax(), color='black', lw=2)
        ax2.axvline(rolling_avg.idxmax() + idx_peak_width/2, color='black', ls='--', lw=2)
        ax2.axvline(rolling_avg.idxmax() - idx_peak_width/2, color='black', ls='--', lw=2)
        plt.show()
        """
        smoothed_scores = scores.rolling(window=idx_peak_width/5, center=True, min_periods=1).mean()
        return smoothed_scores.iloc[idx_peak_width/2:-idx_peak_width/2].idxmax()

    @staticmethod
    def spectral_contrast(x, y):
        return (x*y).sum() / sqrt((x*x).sum() * (y*y).sum())

    @staticmethod
    def lsq_fit(obs_spec, ref_spec):
        # if ref spec is vector x, observed is vector y, solve a*x + b = y and return a
        return linregress(ref_spec, obs_spec)[0]


def get_gaussian_kernel(kernel_len, num_sig):
    interval = (2*num_sig+1.)/kernel_len
    x = np.linspace(-num_sig-interval/2., num_sig+interval/2., kernel_len)
    return st.norm.pdf(x)


class PeakDetectionBoundaryAdjuster(object):
    def __init__(self, chromatogram_files, input_df, reference_label='light', expandtime=1,
                 maxwidthchange=None):
        """
        :param chromatogram_files: a list of chromatogram file locations to process
        :param input_df: a dataframe of peak boundaries
        :param reference_label: which isotope label type to use for the reference peak e.g. light or heavy.
        If set to None -- the label is ignored.  Peptides with two label types will have peak boundaries
        set based on chromatograms from the last label type for that peptide
        :param expandtime: look for a peak within existing boundaries plus expandtime minutes on either side
        :param maxwidthchange: optional threshold on how much a peak width is allowed to change, as a percentage
               compared to the original. If set to None, no threshold is applied. If a peak's width changes too much,
               the original is kept. An example -- if a peaks original width is 30 seconds, and expandtime is set to 10
               any peak with width within 3 seconds of the original peak will be kept
        :return:
        """
        self._chromatogram_files = chromatogram_files
        self._peakboundary_df = input_df
        self._reference_label = reference_label.upper()
        self._expandtime = expandtime
        self._maxwidthchange = maxwidthchange

    def get_boundary_refinement_result(self):
        adjusted_df = self._peakboundary_df.set_index(['file_name', 'peptide_modified_sequence', 'precursor_charge'],
                                                      verify_integrity=True)
        pep_counter = 0
        sys.stderr.write("Processing peptide: 0         ")
        for chrom_file in self._chromatogram_files:
            for chrom_set in stream_chromatograms(chrom_file):
                pep_counter += 1
                sys.stderr.write("\rProcessing peptide: %d         " % pep_counter)
                # a chrom_set is a collection of transitions for a single peak defined by having the same
                # filename, peptide mod seq, precursor charge, and label type
                # check that this label is right
                if self._reference_label and chrom_set[0].IsotopeLabelType.upper() != self._reference_label:
                    continue
                # check if this peptide is in the data frame
                index_entry = (chrom_set[0].FileName,
                               chrom_set[0].PeptideModifiedSequence,
                               int(chrom_set[0].PrecursorCharge))
                try:
                    unadjusted_boundaries = adjusted_df.loc[index_entry, ('min_start_time', 'max_end_time')]
                except KeyError:
                    continue
                # check if the unadjusted boundaries are N/A if so cannot adjust them
                if any(pd.isnull(unadjusted_boundaries)):
                    continue

                detector = PeakBoundaryFinder(unadjusted_boundaries['min_start_time'],
                                              unadjusted_boundaries['max_end_time'],
                                              chrom_set[0].Times,
                                              expandtime=self._expandtime,
                                              maxwidthchange=self._maxwidthchange)
                for trans_data in chrom_set:
                    detector.add_transition_trace(trans_data.FragmentIon,
                                                  int(trans_data.ProductCharge),
                                                  trans_data.Intensities)
                adjusted_df.loc[index_entry, ('min_start_time', 'max_end_time')] = detector.refine_peak_boundary()
        return adjusted_df.reset_index()


class PeakBoundaryFinder(object):
    def __init__(self, start_time, end_time, timestring, expandtime=1, maxwidthchange=None):
        self.fragments_intensity_trace = {}
        self.times = [float(i) for i in timestring.split(',')]
        self.start_time = start_time
        self.end_time = end_time
        self.expandtime = expandtime
        self.threshold = 0.01
        self.increasing_threshold = 3
        # the maximum change in peak width allowable relative to the input boundaries in minutes
        self.maxwidthchange = maxwidthchange
        # self.center = 0

    def add_transition_trace(self, fragmentlabel, fragment_charge, intensitystring):
        if not self.fragments_intensity_trace.has_key(fragmentlabel):
            intensities = [float(i) for i in intensitystring.split(',')]
            key = fragmentlabel + "_" + str(fragment_charge)
            self.fragments_intensity_trace[key] = intensities

    def set_intensity_threshold_ratio(self, threshold=0.01):
        """
        :param threshold: default is 0.01 means 1% of max intensity
        :return:
        """
        self.threshold = threshold

    def set_consecutive_increasing_count(self, count=3):
        self.increasing_threshold = count

    ## this part is from Brian's peak boundary refinement method in encyclopedia
    ## the original idea is in TransitionRefiner.java and PeptideQuantExtractorTask.java
    def refine_peak_boundary(self):
        """
        :return: updated start_time, updated end_time
        """
        intensity_in_window = self.fragments_intensity_trace.copy()
        left_idx = Functions.BinarySearch(self.times, self.start_time - self.expandtime)
        right_idx = Functions.BinarySearch(self.times, self.end_time + self.expandtime)

        ## normalize intensities
        all_normalized = {}
        for frag in intensity_in_window.keys():
            list = intensity_in_window[frag]
            intRaw = list[left_idx:right_idx]

            normalized = [0] * len(intRaw)
            if np.sum(intRaw) > 0:
                smoothed = Functions.PaddedSavitzkyGolaySmooth(intRaw)
                normalized = Functions.NormalizeTrace(smoothed)
            all_normalized[frag] = normalized

        length = right_idx - left_idx
        maxidx = 0
        maxvalue = 0
        medians = []
        ## find the median point across time in all the normalized transitions
        ## find highest point

        for i in range(0, length):
            values = []
            for frag in intensity_in_window.keys():
                values.append(all_normalized[frag][i])

            median = np.median(values)
            medians.append(median)

            if median > maxvalue:
                maxvalue = median
                maxidx = i

        if maxvalue <= 0:
            return self.start_time, self.end_time

        intThreshold = maxvalue * self.threshold
        updatedleft = self.detect_boundary(medians, maxidx, intThreshold, -1)
        updatedright = self.detect_boundary(medians, maxidx, intThreshold, 1)
        new_start = self.times[left_idx + updatedleft]
        new_end = self.times[left_idx + updatedright]

        if self.maxwidthchange:
            original_width = self.end_time - self.start_time
            new_width = new_end - new_start
            allowed_change = (self.maxwidthchange / 100.0) * original_width
            if abs(new_width - original_width) > allowed_change:
                return self.start_time, self.end_time

        self.start_time = new_start
        self.end_time = new_end
        return self.start_time, self.end_time

    def detect_boundary(self, values, startIdx, intThreshold, direction=-1):
        """
        :param values: median values of normalized chromatogram across time; list
        :param startIdx: this is the index of highest point of medians; int
        :param intThreshold: the intensity threshold; float
        :param direction:  -1 means finding left boundary, 1 means finding right boundary; -1 or 1
        :return: index of peak boundary; int
        """
        boundary_idx = startIdx
        increasing = 0
        edge_idx = 0
        if direction == 1:
            edge_idx = len(values)

        for i in range(boundary_idx, edge_idx, direction):
            if values[i] > values[startIdx]:  ##
                increasing += 1
            elif increasing > 0:
                increasing -= 1

            if (increasing >= self.increasing_threshold and values[startIdx] / 2 > values[boundary_idx]):
                break
            if (values[i] < values[boundary_idx]):
                boundary_idx = i
            if (values[i] < intThreshold):
                break

        return boundary_idx
