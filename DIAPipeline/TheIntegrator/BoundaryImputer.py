"""
BoundaryImputer.py
Various classes to impute missing peak boundaries

Author: Jarrett Egertson
"""

import os
import pandas as pd
import numpy as np
from RtAlign import KdeSplineAlignment
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys


class BoundaryImputer(object):
    """Base Boundary imputation class"""

    def __init__(self, input_df):
        self._input_df = input_df

    @staticmethod
    def add_center_rt(df):
        df_ret = df.copy()
        df_ret['center_time'] = (df.min_start_time + df.max_end_time) / 2.0
        return df_ret

    @staticmethod
    def correct_start_stop_order(df, start_col='min_start_time', end_col='max_end_time'):
        """
        Detects instances where the start time for a peak is less than the end time and corrects it
        :param end_col: the string name for the column with the end retention time
        :param start_col: the string name for the column with the starting retention time
        :param df:
        :return: new dataframe with correct start/stop
        """
        fixed_df = df.copy()
        rows_to_flip = df[start_col] > df[end_col]
        fixed_df.loc[rows_to_flip, start_col] = df[rows_to_flip][end_col]
        fixed_df.loc[rows_to_flip, end_col] = df[rows_to_flip][start_col]
        # fixed_df.loc[rows_to_flip, ['min_start_time', 'max_end_time']] = \
        #     df[rows_to_flip][['max_end_time', 'min_start_time']]
        return fixed_df

    def impute(self):
        return self._input_df.copy()


class SplineAlignmentImputer(BoundaryImputer):
    def __init__(self, input_df):
        """
        :param input_df: -- this input dataframe must have entries for every peptide in every file with missing
        boundaries indicated by NaN
        :return:
        """
        super(SplineAlignmentImputer, self).__init__(input_df)
        self._input_df = self.add_center_rt(self._input_df)
        self._rt_similarity_matrix = self.get_rt_similarity_matrix(self._input_df)

    def impute(self, write_plot=False, ax_in=None, outliers_already_removed=False):
        # calculate the center RT for every peak
        imputed_df = self._input_df.copy()
        num_files = len(self._rt_similarity_matrix)
        sys.stderr.write("Imputing boundaries:           ")
        file_counter = 0
        if write_plot:
            pdf = PdfPages('pairwise_rt_alignments.pdf')
        for file_name, group in self._input_df.groupby('file_name'):
            file_counter += 1
            sys.stderr.write("\rImputing boundaries: File %d of %d" % (file_counter, num_files))
            closest_files = self._rt_similarity_matrix[file_name].sort_values(ascending=True).index
            imputed_file = group.copy()
            imputed_file['original_index'] = imputed_file.index
            imputed_file.set_index(['peptide_modified_sequence', 'precursor_charge'], inplace=True)
            for ref_file in closest_files:
                if ref_file == file_name:
                    continue
                needs_impute = imputed_file[pd.isnull(imputed_file['center_time'])].index.values
                if len(needs_impute) == 0:
                    # no more nans, done imputing this file
                    break
                ref_df = self._input_df[self._input_df['file_name'] == ref_file]
                ref_df.set_index(['peptide_modified_sequence', 'precursor_charge'], inplace=True)
                # number of outliers that will be imputed from the ref df (number of non-null entries)
                num_to_impute = ref_df.loc[needs_impute, 'center_time'].count()
                # sometimes, the reference file will not have information for any of the precursors that need imputation
                # in this case, the reference file is skipped. This in particular can happen if the data frame
                # had outliers removed previously using TheIntegrator. If there is no clear majority for imputation,
                # the entire peptide is removed, so all values are N/A
                if num_to_impute == 0:
                    continue
                ax_in = None
                if write_plot:
                    fig = plt.figure()
                    ax_in = fig.add_subplot(111)
                # aligner = SVRAlignment(self._input_df[self._input_df['file_name'] == ref_file],
                #                        self._input_df[self._input_df['file_name'] == file_name],
                #                        outliers_already_removed=outliers_already_removed)
                aligner = KdeSplineAlignment(self._input_df[self._input_df['file_name'] == ref_file],
                                             self._input_df[self._input_df['file_name'] == file_name])
                if write_plot:
                    aligner.write_plot(ax_in)
                    pdf.savefig()
                    plt.close()
                # use a .apply on each of the three RT columns on the subset of values that are missing to imputed_file
                imputed_file.loc[needs_impute, ['center_time', 'min_start_time', 'max_end_time']] = \
                    ref_df.loc[needs_impute, ['center_time', 'min_start_time', 'max_end_time']].apply(aligner.align)
            imputed_df.loc[imputed_file['original_index'].values, ['center_time', 'min_start_time', 'max_end_time']] = \
                imputed_file.loc[:, ['center_time', 'min_start_time', 'max_end_time']].values
        if write_plot:
            pdf.close()
        sys.stderr.write("\rImputing boundaries: Done             " + os.linesep)
        return BoundaryImputer.correct_start_stop_order(imputed_df)

    @staticmethod
    def agg_func_with_exception(x):
        if len(x) > 1:
            raise Exception("Error creating pivot table -- multiple entries for column")
        return x.iloc[0]

    @staticmethod
    def get_rt_similarity_matrix(df):
        # a square matrix with filenames in rows and columns containing pairwise median absolute
        # delta retention times
        pivot = pd.pivot_table(df,
                               values='center_time',
                               index=['peptide_modified_sequence', 'precursor_charge'],
                               columns='file_name',
                               aggfunc=SplineAlignmentImputer.agg_func_with_exception)
        return pivot.apply(
            lambda col1: pivot.apply(lambda col2: SplineAlignmentImputer.get_rt_similarity_by_columns(col1, col2)))

    @staticmethod
    def get_rt_similarity(df, f1, f2):
        """
        Calculate how similar the RT values are between a pair of datasets.
        Calculated as the median absolute(delta RT) between the two
        :param df:
        :param f1: filename 1 for pairwise comparison
        :param f2: filename 2
        :return: the average abs delta RT between the files
        """
        pivot = pd.pivot_table(df[df['file_name'].isin((f1, f2))],
                               values='center_time',
                               index=['peptide_modified_sequence', 'precursor_charge'],
                               columns='file_name',
                               aggfunc=SplineAlignmentImputer.agg_func_with_exception)
        return SplineAlignmentImputer.get_rt_similarity_by_columns(pivot.iloc[:, 0], pivot.iloc[:, 1])

    @staticmethod
    def get_rt_similarity_by_columns(col1, col2):
        return np.nanmedian(np.absolute(col1 - col2))
