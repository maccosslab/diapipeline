"""
BlibUtils.py -- helper functions for dealing with blib files
"""

import struct
import zlib
from sqlalchemy import create_engine
import pandas as pd

# the proteowizard blib implementation writes compressed binary if it's
# smaller than the uncompressed, otherwise it writes uncompressed. It does
# this for both mz and intensity.


def read_blib_mz_blob(blob, numPeaks):
    if len(blob) < numPeaks * 8:
        blob = zlib.decompress(blob)
    return struct.unpack("<" + ('d'*numPeaks), blob)


def read_blib_intensity_blob(blob, numPeaks):
    if len(blob) < numPeaks * 4:
        blob = zlib.decompress(blob)
    return struct.unpack("<" + ('f'*numPeaks), blob)


def write_blib_mz_blob(mz_list):
    # always write compressed mz
    blob = struct.pack('<' + ('d'*len(mz_list)), mz_list)
    return transform_blob(blob)


def write_blib_intensity_blob(intensity_list):
    # always write compressed intensity
    blob = struct.pack('<' + ('f'*len(intensity_list)), intensity_list)
    return transform_blob(blob)


def transform_blob(blob):
    compressed_blob = zlib.compress(blob)
    if len(compressed_blob) < len(blob):
        return compressed_blob
    return blob


class BlibCompressionTypes:
    AlwaysUncompressed, AlwaysCompressed, Conditional, Unknown = range(4)


class BlibBinaryProcessor(object):
    """
    Handles reading and writing of binary data array for the blib format.
    Can automatically infer if blib was written with conditional compression
    (compress binary data array only if compressed size less than uncompressed)
    or always compressed (as done by EncyclopeDIA), or always uncompressed (Pecan)
    """
    MZ_WORD = 8
    INTENSITY_WORD = 4

    def __init__(self, blib_file=None):
        # if a blib file is passed, the compression scheme is inferred from the file
        self._mz_compression = BlibCompressionTypes.Conditional
        self._intensity_compression = BlibCompressionTypes.Conditional
        if blib_file is not None:
            self._mz_compression, self._intensity_compression = \
                self.infer_compression(blib_file)

    def set_mz_compression(self, compression_type):
        """
        Explicitly set the mz compression approach
        :param compression_type: type BlibCompressionTypes
        :return:
        """
        self._mz_compression = compression_type

    def set_intensity_compression(self, compression_type):
        """
        Explicitly set the intensity compression approach
        :param compression_type: type BlibCompressionTypes
        :return:
        """
        self._intensity_compression = compression_type

    @staticmethod
    def infer_compression(blib_file):
        # get a dataframe with the m/z, intensity binary arrays, and numpeaks
        engine = create_engine("sqlite:///%s" % blib_file)
        ref_spec_peaks = pd.read_sql('RefSpectraPeaks', engine)
        ref_spec = pd.read_sql('RefSpectra', engine)
        df = ref_spec.merge(right=ref_spec_peaks, how='left', left_on='id',
                            right_on='RefSpectraID')[['peakMZ', 'peakIntensity', 'numPeaks']]
        df.columns = ['mz', 'intensity', 'num_peaks']
        inferred_mz_compress = \
            BlibBinaryProcessor.evaluate_binary_batch(df,
                                                      BlibBinaryProcessor.MZ_WORD,
                                                      'mz',
                                                      BlibBinaryProcessor.is_mz_reasonable)
        inferred_intensity_compress = \
            BlibBinaryProcessor.evaluate_binary_batch(df,
                                                      BlibBinaryProcessor.INTENSITY_WORD,
                                                      'intensity',
                                                      BlibBinaryProcessor.is_intensity_reasonable)

        if inferred_mz_compress is BlibCompressionTypes.Unknown:
            raise Exception("Unable to determine if m/z compression is conditional or always compressed.")
        if inferred_intensity_compress is BlibCompressionTypes.Unknown:
            raise Exception("Unable to determine if intensity compression is conditional or always compressed.")

        return inferred_mz_compress, inferred_intensity_compress

    @staticmethod
    def evaluate_binary_batch(df_in, expected_length, data_column, data_test_function):
        """
        Evaluate all data blobs in a column of the dataframe (from blib sql) and determine
        if data is compressed always, never, or conditionally
        :param df_in: dataframe with num_peaks column and a column with binary data to evaluate
        :param expected_length: expected length of each peak in bytes
        :param data_column: name of the column containing the binary data
        :param data_test_function: a function to determine if interpreted binary data looks reasonable
        :return: 
        """
        df = df_in.copy()
        df['len_match'] = \
            df.apply(lambda x: x['num_peaks'] * expected_length == len(x[data_column]), axis=1)

        decomp_name = data_column + '_decomp'
        if df['len_match'].all():
            if all(df.apply(data_test_function, axis=1)):
                return BlibCompressionTypes.AlwaysUncompressed
        if not df['len_match'].any():
            try:
                df[decomp_name] = df[data_column].apply(zlib.decompress)
            except zlib.error:
                return BlibCompressionTypes.Unknown
            if all(df.apply(data_test_function, axis=1, col_name=decomp_name)):
                return BlibCompressionTypes.AlwaysCompressed

        df_filter = df[df['len_match']]
        reasonable_without_decomp = df_filter.apply(data_test_function, axis=1)
        if all(reasonable_without_decomp):
            return BlibCompressionTypes.Conditional
        try:
            df_filter[data_column] = df_filter[data_column].apply(zlib.decompress)
        except zlib.error:
            raise Exception("Decompression error: unable to determine if %s compression is "
                            "conditional or always compressed." % data_column)
        reasonable_with_decomp = df_filter.apply(data_test_function, axis=1)
        if all(reasonable_with_decomp):
            return BlibCompressionTypes.AlwaysCompressed
        return BlibCompressionTypes.Unknown

    @staticmethod
    def is_mz_reasonable(x, col_name='mz'):
        """
        Check if a binary data array is a sensical m/z array. Sensical means all m/z values are signed
        the same, and none have absolute value beyond 1e6
        :param x: a pandas Series with num_peaks, and mz columns
        :param col_name: name of the column containing the binary data
        :return: True if mz array is sensical, False if it's not indicating data may be compressed
        """
        num_peaks = x['num_peaks']
        mz_array = x[col_name]
        mz_values = struct.unpack("<" + ('d' * num_peaks), mz_array)
        mz_is_neg = [entry < 0 for entry in mz_values]
        if any(mz_is_neg) and not all(mz_is_neg):
            # m/z array contains both positive and negative values
            return False
        if any([abs(entry) > 1e6 for entry in mz_values]):
            # m/z array contains m/z values with absolute value > 1e6
            return False
        return True

    @staticmethod
    def is_intensity_reasonable(x, col_name='intensity'):
        """
        Check if a binary data array is a sensical intensity array. Sensical means no negative values.
        :param x: a pandas Series with num_peaks, and intensity columns
        :param col_name: name of the column containing the binary data
        :return: True if intensity array is sensical, False if it's not indicating data may be compressed
        """
        num_peaks = x['num_peaks']
        intensity_array = x[col_name]
        intensity_values = struct.unpack("<" + ('f' * num_peaks), intensity_array)
        return all([entry >= 0 for entry in intensity_values])
