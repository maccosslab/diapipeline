"""
MzmlWrapper.py

A wrapper that loads an mzml file into memory for quick queries by precursor m/z and RT

"""
import pymzml
from decimal import Decimal
from collections import namedtuple
from ListHelpers import *


class MzmlWrapper(object):
    def __init__(self, mzml_location):
        # a list of precursor windows
        self._precursor_windows = None
        self._precursor_mzs = None
        self.load_mzml(mzml_location)

    def load_mzml(self, mzml_location):
        prec_dict = dict()
        msrun = pymzml.run.Reader(mzml_location)
        Spectrum = namedtuple("Spectrum", ["mz", "intens"])
        for spectrum in msrun:
            if spectrum['ms level'] != 2:
                continue
            if len(spectrum['precursors']) != 1:
                raise Exception("Do not know how to handle spectrum with more than 1 precursor")
            precursor_mz = Decimal("%.2f" % spectrum['precursors'][0]["mz"])
            if precursor_mz not in prec_dict:
                prec_dict[precursor_mz] = list()
            prec_dict[precursor_mz].append((spectrum['scan start time'], Spectrum(spectrum.mz, spectrum.i)))
        PrecData = namedtuple("PrecData", ["rts", "spectra"])
        self._precursor_mzs = sorted(prec_dict.keys())
        self._precursor_windows = [PrecData(*zip(*(prec_dict[k]))) for k in self._precursor_mzs]
        self._precursor_mzs = [float(entry) for entry in self._precursor_mzs]

    def get_spectrum(self, query_mz, query_rt):
        # find the precursor window
        prec_stack = self._precursor_windows[get_closest(self._precursor_mzs, query_mz)]
        s = prec_stack.spectra[get_closest(prec_stack.rts, query_rt)]
        return s
