"""
ListHelpers.py

A few helper functions to find values in sorted lists
"""

from bisect import bisect_left


def get_closest(a, x):
    """
    # find the index of the closest value to x in a
    Assumes a is sorted
    """
    pos = bisect_left(a, x)
    if pos == 0:
        return 0
    if pos == len(a):
        return len(a)-1
    before = a[pos-1]
    after = a[pos]
    if after - x < x - before:
        return pos
    else:
        return pos-1


def get_closest_value(a, x):
    return a[get_closest(a, x)]

