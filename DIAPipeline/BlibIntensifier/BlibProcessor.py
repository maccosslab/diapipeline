"""
BlibProcessor.py

Class to handle a blib file and update it with intensities from a file set
"""

import multiprocessing
import sqlite3
from collections import namedtuple
import os
from MzmlWrapper import MzmlWrapper
import struct
from ListHelpers import *
import sys


def update_wrapper(*args, **kwargs):
    return BlibProcessor.update_intensities_for_file(*args, **kwargs)


class BlibProcessor(object):
    def __init__(self, blib_file, mzml_files, pool_size=4, ppm_error=10):
        """
        :param blib_file: File path for input blib file
        :param mzml_files: List of mzml files to read spectra from
        :param pool_size: Number of mzml files to process at a time
        :param ppm_error: The mass error tolerance (in ppm) for mapping blib spectra peaks to reference spectra
        :return:
        """
        self._pool_size = pool_size
        self._blib_file = blib_file
        self._mzml_files = mzml_files
        self._ppm_error = ppm_error

    def go(self):
        multiprocessing.log_to_stderr()
        pool = multiprocessing.Pool(processes=self._pool_size)
        spec_files_to_process = self.find_reference_spectra()
        for spec_file in spec_files_to_process:
            pool.apply_async(update_wrapper, args=(self._blib_file,
                                                   spec_file.file_id,
                                                   spec_file.file_name,
                                                   self._ppm_error))
        pool.close()
        pool.join()
        return

    def get_files_from_blib(self):
        """
        :return: The set of spectra files written in the .blib file
        """
        conn = sqlite3.connect(self._blib_file)
        c = conn.cursor()
        blib_files = c.execute('SELECT * from SpectrumSourceFiles').fetchall()
        BlibMzmlEntry = namedtuple("BlibFileEntry", ["file_id", "file_name"])
        conn.close()
        return [BlibMzmlEntry(*entry) for entry in blib_files]

    def find_reference_spectra(self):
        """
        Try to find the reference spectra files referred to in the blib file
        :return: a list of ResolvedRefFile objects w/ file_id and file_name attributes
        """
        blib_file_set = self.get_files_from_blib()
        resolved_blib_files = list()
        for file_id, file_name in blib_file_set:
            blib_basename = os.path.basename(file_name)
            blib_root = os.path.splitext(blib_basename)[0]
            match_file = None
            for ref_file in self._mzml_files:
                ref_basename = os.path.basename(ref_file)
                ref_root = os.path.splitext(ref_basename)[0]
                if blib_root == ref_root:
                    match_file = ref_file
                    break
            if not match_file:
                raise Exception("Could not find a matching reference file for blib spec file: %s" % file_name)
            ResolvedRefFile = namedtuple("ResolvedRefFile", ["file_id", "file_name"])
            resolved_blib_files.append(ResolvedRefFile(file_id, match_file))
        return resolved_blib_files

    @staticmethod
    def update_intensities_for_file(blib_file, mzml_file_id, mzml_file, ppm_error):
        """
        :param blib_file: .blib file to update
        :param mzml_file_id: the file id for the mzml_file in the blib file
        :param mzml_file: the path of the mzml file on the local filesystem
        :param ppm_error: ppm error for matching peaks
        :param logger: logging object
        :return:
        """
        print "Started processing file %s" % os.path.basename(mzml_file)
        sys.stdout.flush()
        mzml_wrapped = MzmlWrapper(mzml_file)
        conn = sqlite3.connect(blib_file)
        c = conn.cursor()
        # get the reference spectra for this file
        # need ref row id, precursor mz, RT, peakmzs_blob
        spectra_to_process = \
            c.execute("SELECT RS.id, RS.precursorMZ, RS.numPeaks, RS.retentionTime, RP.peakMZ FROM "
                      "RefSpectra AS RS JOIN RefSpectraPeaks AS RP "
                      "ON RS.id=RP.RefSpectraID "
                      "WHERE RS.fileID= ? ", (mzml_file_id,)).fetchall()
        SpectrumFromBlib = namedtuple("SpectrumFromBlib", ["spec_id",
                                                           "precursor_mz",
                                                           "num_peaks",
                                                           "rt",
                                                           "peak_mzs"])
        spectra_to_process = [SpectrumFromBlib(*entry) for entry in spectra_to_process]
        # processed_spectra is a list of tuples (spec_id, blobbed intensities)
        processed_spectra = [BlibProcessor.adjust_blib_spectrum(b_spec, ppm_error, mzml_wrapped) for b_spec in
                             spectra_to_process]
        c.executemany("UPDATE RefSpectraPeaks SET peakIntensity=? WHERE RefSpectraId=?", processed_spectra)
        conn.commit()
        conn.close()
        print "Finished processing file %s" % os.path.basename(mzml_file)
        sys.stdout.flush()

    @staticmethod
    def adjust_blib_spectrum(blib_spectrum, ppm_error, mzml):
        """
        Find the peak intensities for the corresponding blib spectrum
        :param blib_spectrum: SpectrumFromBlib object with spec_id, precursor_mz, num_peaks, rt, peak_mzs attributes
        :param ppm_error: mass error in ppm for matching reference spectrum peak to bibliospec mz
        :param mzml: MzmlWrapper object for finding matching spectrum
        :return: blib spectrum id and new intensity blob
        """
        # find the matching spectrum
        matched_spec = mzml.get_spectrum(blib_spectrum.precursor_mz, blib_spectrum.rt)
        BlibIntensities = namedtuple("BlibIntensities", ["intensities", "spec_id"])
        new_intensities = BlibProcessor.rip_peaks(blib_spectrum.peak_mzs,
                                                  blib_spectrum.num_peaks,
                                                  matched_spec,
                                                  ppm_error)
        return BlibIntensities(new_intensities, blib_spectrum.spec_id)

    @staticmethod
    def rip_peaks(b_mz, b_len, ref_spec, ppm_error):
        """
        :param b_mz: bibliospec mz values (blobbed)
        :param b_len: length of bibliospec blobs
        :param ref_spec: reference spectrum (namedtuple -- mz, intens)
        :param ppm_error: ppm error for matching blib peak to ref spectrum
        :return: new blobbed intensities
        """
        blib_mzs = struct.unpack('d' * b_len, b_mz)
        ripped_intensities = [0.0] * b_len
        for i, q_mz in enumerate(blib_mzs):
            closest_mz_index = get_closest(ref_spec.mz, q_mz)
            closest_mz = ref_spec.mz[closest_mz_index]
            if abs(1e6 * (q_mz - closest_mz) / q_mz) < ppm_error:
                ripped_intensities[i] = ref_spec.intens[closest_mz_index]
        return buffer(struct.pack('f' * b_len, *ripped_intensities))
