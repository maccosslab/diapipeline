"""
BlibRetentionator.py

A script to merge a peak boundaries file into a .blib file.
"""

import argparse
import os
import shutil

from DIAPipeline.BlibRetentionator.BlibRetentionator import BlibRetentionator
from DIAPipeline.scripts.DiaPipelineTheIntegrator import IntegrationManager


def init_argparse():
    parser = argparse.ArgumentParser(
        description="BlibRetentionator injects retention times from a Skyline peak boundaries file into "
                    "an existing blib library.  It is designed to work with the specific flavor of blibs "
                    "output by Encyclopedia.  [peptide, precursor charge] combinations that are in the integration "
                    "boundaries file but not the blib file will be ignored.  If addNew is specified, BlibRetentionator "
                    "will add retention time entries for new files so long as the [peptide, precursor charge] "
                    "combo already exists in the blib file.  If addNew is not used, BlibRetentionator will only update "
                    "retention times for existing peptide, precursor charge, filename combinations",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--addNew', default=False, action='store_true')
    parser.add_argument('inputBlib', type=os.path.abspath, help="Input blib file")
    parser.add_argument('inputBoundaries', type=os.path.abspath, help="Text file with a list of"
                                                                      "integration boundaries files"
                                                                      "to use as a source for retention times"
                                                                      "to retentionate the blib file with")
    parser.add_argument('outputBlib', type=os.path.abspath, help="Output blib file")
    return parser


def validate_args(args):
    if args.inputBlib == args.outputBlib:
        raise NotImplementedError("The output blib file must be different than the input blib.")


def main():
    parser = init_argparse()
    args = parser.parse_args()
    validate_args(args)
    im = IntegrationManager()
    input_boundaries = list()
    with open(args.inputBoundaries) as fin:
        for line in fin:
            input_boundaries.append(os.path.abspath(line.strip()))
    for fname in input_boundaries:
        im.add_boundaries_file(fname)

    merged_boundaries = im.get_merged_boundaries()
    shutil.copy(args.inputBlib, args.outputBlib)
    br = BlibRetentionator(args.outputBlib, args.addNew)
    br.process_boundaries_frame(merged_boundaries)
    br.commit()


if __name__ == "__main__":
    main()
