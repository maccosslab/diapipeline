"""
DiaPipelineBlibIntensifier.py

Reads in a Pecan blib file with theoretical spectrum intensities and writes out a new file with experimental
intensities by referencing the input spectra file.
"""

import argparse
import os.path
import shutil
from DIAPipeline.BlibIntensifier.BlibProcessor import BlibProcessor
import multiprocessing


def init_argparse():
    parser = argparse.ArgumentParser(
        description="Read in a Pecan blib file with theoretical spectrum intensities and write a new blib with "
                    "experimental intensities ripped from reference spectra files.  Assumes constant isolation width "
                    "for all windows (but they may overlap)",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--ppmError', type=float, default=10.0, help="PPM error for matching peaks to pecan library")
    parser.add_argument('--referenceDirectory', type=os.path.abspath, default=os.getcwd(),
                        help="Folder containing reference mzML files")
    parser.add_argument('--numProcesses', type=int, default=multiprocessing.cpu_count(),
                        help="Number of processes to spawn concurrently, default = # cpus")
    parser.add_argument('inputBlib', type=os.path.abspath, help="Input blib file")
    parser.add_argument('outputBlib', type=os.path.abspath, help="Output blib file")
    return parser


def main():
    parser = init_argparse()
    args = parser.parse_args()

    # copy the input blib file to the output blib
    if args.inputBlib == args.outputBlib:
        raise Exception("Input blib cannot be the same as the output blib")

    shutil.copy(args.inputBlib, args.outputBlib)

    mzml_list = os.listdir(args.referenceDirectory)
    mzml_list = [f for f in mzml_list if os.path.splitext(f)[-1].upper() == ".MZML"]
    mzml_list = [os.path.join(args.referenceDirectory, f) for f in mzml_list]

    bp = BlibProcessor(args.outputBlib, mzml_list, pool_size=args.numProcesses)
    bp.go()


if __name__ == "__main__":
    main()
