import pandas as pd
import argparse
import os

from DIAPipeline.TheIntegrator.IntegrationManager import IntegrationManager
from DIAPipeline.TheIntegrator.BoundaryImputer import SplineAlignmentImputer
from DIAPipeline.TheIntegrator.BoundaryAdjustmentManager import FixedWidthBoundaryAdjuster


def init_argparse():
    parser = argparse.ArgumentParser(
        description="This script tries to generate decent integration boundaries for a spike-in dataset. To calculate "
                    "boundaries some manual peak picking needs to be done in Skyline first. First, integrate a reference "
                    "set of peptides (recommend 10 or more), in every run in the Skyline document and group them all under "
                    "a single protein in Skyline. These reference peptides are used for retention time alignment. Then,"
                    " pick peaks for all peptides in one of the spike-in runs. Export the peak boundaries in Skyline"
                    "by clicking File -> Export -> Report, and select Peak Boundaries. Also export the chromatograms by"
                    "selecting File -> Export -> Chromatograms... Make sure all files are selected and just product "
                    "chromatograms (not precursor, TIC or Base peak) selected. All peak boundaries for a peptide are "
                    "fixed to be the same width. If Austin is sitting next to you, flick"
                    " his ear for good luck. If he is nearby, but out of arm's reach, throw something in lieu of "
                    "flicking. If still further, send a mean or condescending email to atkeller@uw.edu.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--rtTolerance', default=0.1, type=float,
                        help="To find a peak in a new file, the algorithm first imputes the location by RT alignment "
                             "from the peptide in the reference file. This parameter specifies how far to search"
                             " around this imputed peak center (+/- rtTolerance minutes) to find the new peak "
                             "in the data")
    parser.add_argument("referenceProtein", help="The name of the protein containing the reference peptides for"
                                                 "RT alignemnt.")
    parser.add_argument("referenceRun", help="The name of the replicate in Skyline for which peaks were "
                                             "manually picked.")
    parser.add_argument("inputPeakBoundaries", type=os.path.abspath, help="Input peak boundaries from Skyline")
    parser.add_argument("inputChromatograms", type=os.path.abspath, help="Input chromatograms from Skyline")
    parser.add_argument("outputBoundaries", type=os.path.abspath, help="Output peak boundaries file (csv)")
    return parser


def main():
    parser = init_argparse()
    args = parser.parse_args()

    df = pd.read_csv(args.inputPeakBoundaries)
    # remove anything that isn't reference peptide or run
    df_filtered = df[(df['Protein Name'] == args.referenceProtein) | (df['Replicate Name'] == args.referenceRun)]
    df_filtered.to_csv('scrubbed_boundaries.csv')

    im = IntegrationManager()
    im.add_boundaries_file('scrubbed_boundaries.csv')
    merged = im.get_merged_boundaries()
    bi = SplineAlignmentImputer(merged)
    imputed = bi.impute(write_plot=True, outliers_already_removed=False)
    adjuster = FixedWidthBoundaryAdjuster([args.inputChromatograms],
                                          imputed,
                                          reference_files=[args.referenceRun])
    output_boundaries = adjuster.get_boundary_refinement_result()
    IntegrationManager.write_boundaries_file(output_boundaries, args.outputBoundaries)


if __name__ == '__main__':
    main()
