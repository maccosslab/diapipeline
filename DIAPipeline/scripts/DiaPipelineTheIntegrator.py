"""
DiaPipelineTheIntegrator.py
Author: Jarrett Egertson

Reads in a stack of integration boundary files output from Encyclopedia and merges them into one big peak boundary
file for Skyline.  Optionally does imputation of missing peak boundaries and/or correction of boundaries based on other
reference files.
"""

import argparse
import os
import sys
from DIAPipeline.TheIntegrator.IntegrationManager import IntegrationManager
from DIAPipeline.TheIntegrator.BoundaryImputer import SplineAlignmentImputer
from DIAPipeline.TheIntegrator.BoundaryAdjustmentManager import PeakDetectionBoundaryAdjuster
from DIAPipeline.TheIntegrator.OutlierRemover import OutlierRemover


def init_argparse():
    parser = argparse.ArgumentParser(
        description="TheIntegrator reads in a set of integration boundary files output from encyclopedia or Skyline "
                    "and merges them into a single integration boundary file.  It will also optionally impute "
                    "integration boundaries and correct outlier boundaries ",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--impute', default=False, action='store_true',
                        help="Enable imputation of peak boundaries that are missing for certain runs")
    parser.add_argument('--imputeSkipOutlierTest', default=False, action='store_true',
                        help="This argument only does anything if --removeOutliers is not already specified. "
                             "Use this argument if you are imputing a data set which already has had outliers removed. "
                             "Normally, the imputation algorithm does a coarse first pass removal of outliers prior to "
                             "retention time alignment used for imputation. If this option is specified, this step is "
                             "skipped resulting in a speed up. "
                             "If --removeOutliers is specified, this argument is automatically enabled")
    parser.add_argument('--removeOutliers', default=False, action='store_true',
                        help="Detect and remove outlier peak boundaries")
    parser.add_argument('--writePlots', default=False, action='store_true',
                        help='Writes out a .pdf for each pairwise retention time alignment when '
                             'used in conjunction with the --impute flag')
    parser.add_argument('--writeIntermediate', default=False, action='store_true',
                        help='Writes out peak boundaries for intermediate steps')
    parser.add_argument('--repickBoundaries', default=None, type=os.path.abspath,
                        help="Reanalyze the chromatogram data to repick boundaries.  Pass a text file with a list "
                             "of the chromatogram files from the runs being analyzed.  If chromatogram files are "
                             "missing, boundaries from those files will not be repicked.")
    parser.add_argument('--repickExpandTime', default=60.0, type=float,
                        help="Only used when repickBoundaries is set. When repicking peak boundaries, peaks can be "
                             "searched in a region wider than the current peak boundaries. This parameter specifies"
                             " how much to expand the existing boundaries when searching a peak, in seconds.")
    parser.add_argument('--repickMaxWidthChange', default=None, type=float,
                        help="If this parameter is set, repicked peak boundaries are only accepted if the new peak "
                             " width is within X percent of the original peak width.")
    parser.add_argument('inputFiles', type=os.path.abspath, help="Text file with list of integration files "
                                                                 "to merge. If there are "
                                                                 "multiple entries for a given precursor+ "
                                                                 "filename combination, the boundaries from "
                                                                 "the first entry are kept")
    parser.add_argument('outputFile', type=os.path.abspath, help="Integration files output")
    return parser


def main():
    parser = init_argparse()
    args = parser.parse_args()

    im = IntegrationManager()
    input_files = list()
    with open(args.inputFiles) as fin:
        for line in fin:
            input_files.append(os.path.abspath(line.strip()))
    for fname in input_files:
        im.add_boundaries_file(fname)

    output_boundaries = im.get_merged_boundaries()
    if args.writeIntermediate:
        out_name = os.path.splitext(args.outputFile)[0] + '_inter_0_merged.csv'
        IntegrationManager.write_boundaries_file(output_boundaries, out_name)

    if args.removeOutliers:
        sys.stderr.write("Removing outliers:      ")
        o_r = OutlierRemover(output_boundaries, write_plots=args.writePlots)
        output_boundaries = o_r.remove_outliers()
        sys.stderr.write("\rRemoving outliers: Done!" + os.linesep)
        if args.writeIntermediate:
            out_name = os.path.splitext(args.outputFile)[0] + '_inter_1_no_outliers.csv'
            IntegrationManager.write_boundaries_file(output_boundaries, out_name)

    if args.impute:
        bi = SplineAlignmentImputer(output_boundaries)
        outliers_already_removed = False
        if args.removeOutliers or args.imputeSkipOutlierTest:
            outliers_already_removed = True
        output_boundaries = bi.impute(write_plot=args.writePlots, outliers_already_removed=outliers_already_removed)
        if args.writeIntermediate:
            out_name = os.path.splitext(args.outputFile)[0] + '_inter_2_imputed.csv'
            IntegrationManager.write_boundaries_file(output_boundaries, out_name)

    if args.repickBoundaries:
        chrom_files = list()
        with open(args.repickBoundaries) as fin:
            for line in fin:
                chrom_files.append(os.path.abspath(line.strip()))
        adjuster = PeakDetectionBoundaryAdjuster(chrom_files, output_boundaries, expandtime=args.repickExpandTime / 60.0,
                                                 maxwidthchange=args.repickMaxWidthChange)
        output_boundaries = adjuster.get_boundary_refinement_result()

    IntegrationManager.write_boundaries_file(output_boundaries, args.outputFile)


if __name__ == "__main__":
    main()
