"""
DiaPipelineElibBoundaries.py
Original Author: Jarrett Egertson
Author Date: 9/9/2016

This script reads in a .elib file and writes out an integration boundaries file suitable for use in Skyline.
"""

import argparse
from sqlalchemy import create_engine
import pandas as pd
import os
import logging
from DIAPipeline.ModFix.ModFix import convert_modstring_to_pb


def init_argparse():
    parser = argparse.ArgumentParser(
        description="DiaPipelineElibBoundaries reads peak boundaries from an elib file and writes out a flat "
                    "peakboundaries file suitable for use in Skyline.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--overwrite', default=False, action='store_true',
                        help="Overwrite the output peak boundaries file if it already exists")
    parser.add_argument("inputElibFile", type=os.path.abspath, help="Input elib file")
    parser.add_argument("outputBoundariesFile", type=os.path.abspath, help="Output peak boundaries file")
    return parser


def main():
    logging.basicConfig(level=logging.INFO)
    parser = init_argparse()
    args = parser.parse_args()
    if os.path.exists(args.outputBoundariesFile) and not args.overwrite:
        raise Exception("Output peak boundaries file %s already exists! Aborting." % args.outputBoundariesFile)
    # establish a connection to the elib database
    engine = create_engine("sqlite:///%s" % args.inputElibFile)
    entries_df = pd.read_sql_table('entries', engine)
    peak_boundaries_df = entries_df[['SourceFile',
                                     'PeptideModSeq',
                                     'RTInSecondsStart',
                                     'RTInSecondsStop',
                                     'PrecursorCharge']]
    peak_boundaries_df = peak_boundaries_df.assign(PrecursorIsDecoy="FALSE")
    # peak_boundaries_df.loc[:, "PrecursorIsDecoy"] = "FALSE"
    peak_boundaries_df.loc[:, "RTInSecondsStart"] = peak_boundaries_df['RTInSecondsStart'] / 60.0
    peak_boundaries_df.loc[:, "RTInSecondsStop"] = peak_boundaries_df['RTInSecondsStop'] / 60.0
    peak_boundaries_df.loc[:, "PeptideModSeq"] = peak_boundaries_df['PeptideModSeq'].apply(convert_modstring_to_pb)

    # calculate statistics on peak boundaries file
    num_files = len(peak_boundaries_df.groupby(['SourceFile']))
    counts_per_precursor = peak_boundaries_df.groupby(['PeptideModSeq', 'PrecursorCharge']).size()
    num_precursors = len(counts_per_precursor)
    complete_precursors = len(counts_per_precursor[counts_per_precursor == num_files])
    logging.info("Total number of files: %d" % num_files)
    logging.info("Total number of precursors: %d" % num_precursors)
    logging.info("Total number of precursors with an entry for every file: %d (%.1f %%)" % (
        complete_precursors, 100.0 * complete_precursors / float(num_precursors)))
    skyline_pb_header = ['File Name',
                         'Peptide Modified Sequence',
                         'Min Start Time',
                         'Max End Time',
                         'Precursor Charge',
                         'PrecursorIsDecoy']
    peak_boundaries_df.to_csv(args.outputBoundariesFile, header=skyline_pb_header, index=False)
    print "Done"


if __name__ == "__main__":
    main()
