"""
DiaPipelinePeakBoundaryModFix.py
Original Author: Jarrett Egertson
Author Date: 9/12/2016

This script reads in a peak boundaries file and updates the peptide modified sequences to match
the formatting expected by Skyline
"""

import argparse
import os
import logging
import pandas as pd

from DIAPipeline.ModFix.ModFix import convert_modstring_to_pb


def init_argparse():
    parser = argparse.ArgumentParser(
        description="DiaPipelinePeakBoundaryModFix.py corrects modification incomaptibilty between encyclopedia "
                    "skyline peak boundaries files. This is only required for older versions of Encyclopedia,"
                    "Skyline expects integer (ex. [+57]) mods and also doesn't want to see an asterisk at teh end of a "
                    "peptide sequence to indicate the end of a protein sequence",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--overwrite', default=False, action='store_true',
                        help="Overwrite the output elib file if it already exists")
    parser.add_argument("inputPbFile", type=os.path.abspath, help="Input peak boundary file")
    parser.add_argument("outputPbFile", type=os.path.abspath, help="Output peak boundary file")
    return parser


def main():
    logging.basicConfig(level=logging.INFO)
    parser = init_argparse()
    args = parser.parse_args()
    if os.path.exists(args.outputPbFile) and not args.overwrite:
        raise Exception("Output peak boundaries file %s already exists! Aborting." % args.outputPbFile)
    input_df = pd.read_csv(args.inputPbFile, sep=None, engine='python')
    input_df.loc[:, 'Peptide Modified Sequence'] = input_df['Peptide Modified Sequence'].apply(convert_modstring_to_pb)
    input_df.to_csv(args.outputPbFile, na_rep="#N/A", index=False)
    logging.info("Done")


if __name__ == "__main__":
    main()
