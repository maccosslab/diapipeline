"""
DiaPipelineTICNormalize.py
Original Author: Jarrett Egertson
Author Date: 9/18/2016

Normalize data output in a Skyline report by TIC values computed from imported mzML files.
"""

import argparse
import pandas as pd
import os
import logging
import pymzml


def init_argparse():
    parser = argparse.ArgumentParser(
        description="TICNormalize normalizes peak areas contained in a skyline report by the TIC"
                    "for each file calculated from mzML files",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--overwrite', default=False, action='store_true',
                        help="Overwrite the output report file if it already exists")
    parser.add_argument('--rewriteCache', default=False, action='store_true',
                        help="Ignore cached tic calculations if they exist, recalculate, and rewrite")
    parser.add_argument('inputReport', type=os.path.abspath, help="Input skyline report")
    parser.add_argument('mzmlDirectory', type=os.path.abspath,
                        help="Directory containing mzML files to calculate TIC from")
    parser.add_argument('outputReport', type=os.path.abspath, help="Output skyline report")
    return parser


def find_mzml_files(df, mzml_dir):
    """
    Find a mapping between dataframe entry and mzml file
    :param df: input data frame from skyline report
    :param mzml_dir:
    :return: mapping of data frame file entries to mzml names
    """
    mzml_refs = os.listdir(mzml_dir)
    mzml_refs = [f for f in mzml_refs if os.path.splitext(f)[-1].upper() == ".MZML"]
    # see which mzmls are needed from the report files
    fname_col = "File Name"
    if fname_col not in df.columns:
        fname_col = "Replicate Name"
    if fname_col not in df.columns:
        raise Exception("Could not read file names from skyline report file."
                        "No 'File Name' or 'Replicate' columns were found")
    if fname_col == "Replicate Name":
        logging.info("Using Replicate Name column for finding reference mzMLs")
    query_names = df[fname_col].unique()
    if fname_col == 'File Name':
        # if file name, check if basenames match
        match_f = lambda q, ref: os.path.basename(q) == os.path.basename(ref)
    if fname_col == 'Replicate Name':
        # if replicate name search by checking if mzml file contains replicate name
        match_f = lambda q, ref: ref.count(q) == 1
    mapping = dict()
    for query in query_names:
        matches = [ref for ref in mzml_refs if match_f(query, ref)]
        if len(matches) == 0:
            mapping[query] = None
            logging.warning("Could not find an mzML file for %s" % query)
            continue
        if len(matches) > 1:
            mapping[query] = None
            logging.warning("Found multiple (%d) matches for %s" % (len(matches), query) +
                            "normalization will be skipped for this file")
            continue
        mapping[query] = os.path.join(mzml_dir, matches[0])
    mapping_df = pd.DataFrame.from_dict(mapping, orient='index')
    mapping_df.columns = ["mzml_location"]
    return df.join(mapping_df, on=fname_col)


def get_average_tic(mzml_loc):
    """
    Calculate the average TIC from MS1 scans of the input mzml file
    :param mzml_loc: file location of the mzml
    :return: tic
    """
    included_spectra = 0
    summed_tic = 0.0
    reader = pymzml.run.Reader(mzml_loc)
    for s in reader:
        if s['ms level'] != 1:
            continue
        included_spectra += 1
        summed_tic += s['total ion current']
    if included_spectra == 0:
        logging.warn("No MS1 spectra found for %s, skipping normalization" % os.path.basename(mzml_loc))
        return None
    return summed_tic / float(included_spectra)


def get_tic_normalized_df(df, mzml_dir, rewrite_cache=False):
    # update the dataframe with the location of mzml files referenced in original
    df_norm = find_mzml_files(df, mzml_dir)
    mzml_tics = dict()
    cache_location = os.path.join(mzml_dir, 'calculated_tics.csv')
    tic_cache = None
    if os.path.exists(cache_location) and not rewrite_cache:
        tic_cache = pd.DataFrame.from_csv(cache_location, index_col='mzml')
        # tic_cache.set_index('mzml_location')
    for mzml_file in df_norm['mzml_location'].unique():
        # mzml_tics[mzml_file] = np.random.normal(50000, 1000.0)
        # continue
        if tic_cache is not None and os.path.basename(mzml_file) in tic_cache.index:
            tic = tic_cache.loc[os.path.basename(mzml_file), 'tic']
            logging.info("Recovered TIC for %s from previously computed " % os.path.basename(mzml_file) +
                         "results in calculated_tics.csv")
        else:
            logging.info("Reading file: %s" % os.path.basename(mzml_file))
            tic = get_average_tic(mzml_file)
        mzml_tics[mzml_file] = tic
    df_tics = pd.DataFrame.from_dict(mzml_tics, orient='index')
    df_tics.columns = ['tic']
    df_tics = df_tics.assign(mzml=[os.path.basename(entry) for entry in df_tics.index])
    df_tics[['mzml', 'tic']].to_csv(os.path.join(mzml_dir, 'calculated_tics.csv'), index=False)
    # add the average tic values to the data frame
    df_norm = df_norm.join(df_tics['tic'], on='mzml_location')
    df_norm = df_norm.assign(normalization_factor=df_norm['tic'].max() / df_norm['tic'])
    df_norm = df_norm.assign(normalized_intensity=df_norm['Total Area Fragment'] * df_norm['normalization_factor'])
    df_norm = df_norm.drop('mzml_location', 1)
    return df_norm


def get_report_df(csv_path):
    """
    Reads a Skyline csv report into a pandas data frame
    :param csv_path: csv report file location
    :return: pandas data frame
    """
    df = pd.read_csv(csv_path, sep=None, engine='python')
    # cols = df.columns
    # cols = cols.map(lambda x: x.replace(' ', '_') if isinstance(x, (str, unicode)) else x)
    # df.columns = cols
    return df


def main():
    logging.basicConfig(level=logging.INFO)
    parser = init_argparse()
    args = parser.parse_args()
    if os.path.exists(args.outputReport) and not args.overwrite:
        raise Exception(u"Output report file {0:s} already exists. ".format(args.outputReport) +
                        "Use --overwrite to overwrite. Aborting.")
    df = get_report_df(args.inputReport)
    df_norm = get_tic_normalized_df(df, args.mzmlDirectory, rewrite_cache=args.rewriteCache)
    # see if file was created while TICs were being calculated
    if os.path.exists(args.outputReport) and not args.overwrite:
        raise Exception(u"Output report file {0:s} already exists. ".format(args.outputReport) +
                        "Use --overwrite to overwrite. Aborting.")
    df_norm.to_csv(args.outputReport, na_rep="#N/A", index=False)


if __name__ == '__main__':
    main()
