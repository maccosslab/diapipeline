"""
DiaPipelineBlibModFix.py
Original Author: Jarrett Egertson
Author Date: 9/12/2016

This is a very specific script designed to aid in making .blib files compatible with Skyline. It does a find and replace
operation on the Peptide Modified Sequence entry in a .blib file.
"""

import argparse
import os
import pandas as pd
from sqlalchemy import create_engine, MetaData
import logging
import shutil

from DIAPipeline.ModFix.ModFix import convert_modstring_to_blib


def init_argparse():
    parser = argparse.ArgumentParser(
        description="DiaPipelineBlibModFix.py corrects modification incomaptibilty between encyclopedia .blib and skyline .blib "
                    "this is only required for older versions of Encyclopedia, skyline likes .blib mods written with"
                    "one decimal precision (ex. [+57.0]) and also doesn't want to see an asterisk at teh end of a "
                    "peptide sequence to indicate the end of a protein sequence",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--overwrite', default=False, action='store_true',
                        help="Overwrite the output elib file if it already exists")
    parser.add_argument("inputBlibFile", type=os.path.abspath, help="Input blib file")
    parser.add_argument("outputBlibFile", type=os.path.abspath, help="Output blib file")
    return parser


def get_dtypes(tablename, engine):
    meta = MetaData(engine, True)
    table = meta.tables[tablename]
    dtypes = dict()
    for col in table.columns:
        dtypes[col.name] = col.type
    return dtypes


def main():
    logging.basicConfig(level=logging.INFO)
    parser = init_argparse()
    args = parser.parse_args()
    if os.path.exists(args.outputBlibFile) and not args.overwrite:
        raise Exception("Output blib file %s already exists! Aborting." % args.outputBlibFile)
    shutil.copy(args.inputBlibFile, args.outputBlibFile)
    engine = create_engine("sqlite:///%s" % args.outputBlibFile)
    entries_df = pd.read_sql_table('RefSpectra', engine)
    entries_dtypes = get_dtypes('RefSpectra', engine)
    entries_df.loc[:, 'peptideModSeq'] = entries_df['peptideModSeq'].apply(convert_modstring_to_blib)
    entries_df.to_sql('RefSpectra', engine, if_exists='replace', dtype=entries_dtypes, index=False)
    logging.info("Done.")


if __name__ == "__main__":
    main()
