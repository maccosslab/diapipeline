from setuptools import setup, find_packages
from subprocess import check_output


def get_version_string():
    output = check_output(["git", "describe", "--tags"])
    parts = output.split('-')
    tag, count, sha = parts[:3]
    return "{}.dev{}+{}".format(tag, count, sha)

setup(
    name="DIAPipeline",
    version=get_version_string(),
    description="Jarrett's DIA Pipeline",
    author="Jarrett Egertson",
    author_email="jegertso@uw.edu",
    classifiers=['Development Status :: 3 - Alpha',
                 'Programming Language :: Python :: 2.7'],
    zip_safe=False,
    packages=find_packages(),
    install_requires=['matplotlib',
                      'seaborn',
                      'pandas',
                      'pymzml',
                      'numpy',
                      'scipy',
                      'scikit-learn',
                      'sqlalchemy',
                      'statsmodels',
                      'tqdm'],
    entry_points={
        'console_scripts': [
            'BlibDesearleinator = DIAPipeline.scripts.DiaPipelineBlibDesearleinator:main',
            'BlibIntensifier = DIAPipeline.scripts.DiaPipelineBlibIntensifier:main',
            'BlibModFix = DIAPipeline.scripts.DiaPipelineBlibModFix:main',
            'BlibRetentionator = DIAPipeline.scripts.DiaPipelineBlibRetentionator:main',
            'ElibBoundaries = DIAPipeline.scripts.DiaPipelineElibBoundaries:main',
            'FixLoqBoundaries = DIAPipeline.scripts.DiaPipelineFixLoqBoundaries:main',
            'PeakBoundaryModFix = DIAPipeline.scripts.DiaPipelinePeakBoundaryModFix:main',
            'TheIntegrator = DIAPipeline.scripts.DiaPipelineTheIntegrator:main',
            'TICNormalize = DIAPipeline.scripts.DiaPipelineTICNormalize:main'
        ]
    },)
